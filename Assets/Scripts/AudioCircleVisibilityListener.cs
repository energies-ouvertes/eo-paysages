﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages
{
	///<summary>
	/// Legacy code.
	/// Enables or disables the game object based on AROverlaysSettings.AudioCirclesVisibility.
	///</summary>
	public class AudioCircleVisibilityListener : MonoBehaviour
	{
		void Start()
		{
			AROverlaysSettings.AudioCirclesVisibilityChanged.AddListener(refreshActive);
			refreshActive();
		}

		void refreshActive()
		{
			gameObject.SetActive(AROverlaysSettings.AudioCircles);
		}
	}
}
