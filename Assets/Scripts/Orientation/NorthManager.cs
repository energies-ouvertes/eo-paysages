// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace EOPaysages.Orientation
{
	///<summary>
	/// Contains the direction to geographic North and fires an event when it changes.
	///</summary>
	public class NorthManager : MonoBehaviour
	{
		static NorthManager _instance = null;
		public static NorthManager Instance
		{
			get
			{
				if (_instance == null)
				{
					GameObject obj = new GameObject("NorthManager");
					_instance = obj.AddComponent<NorthManager>();
				}
				return _instance;
			}
		}

		public readonly UnityEvent NorthUpdated = new UnityEvent();

		public readonly UnityEvent NorthLost = new UnityEvent();

		///<summary>
		/// Difference from virtual AR North to virtual North (0, 0, 1), in degrees.
		///</summary>
		public float North
		{
			get => _north;
			set
			{
				_north = value;
				UnityEngine.Debug.Log($"North angle updated: {_north}");
				NorthUpdated.Invoke();
			}
		}

		float _north = 0;

		bool trackingLost = true;

		ARSession arSession;

		void Awake()
		{
			arSession = FindObjectOfType<ARSession>();
		}

		void Update()
		{
			XRSessionSubsystem sessionSubsystem = arSession.subsystem;
			if (sessionSubsystem != null)
			{
				NotTrackingReason notTrackingReason = sessionSubsystem.notTrackingReason;
				if (notTrackingReason != NotTrackingReason.None) // Tracking lost
				{
					if (!trackingLost)
					{
						trackingLost = true;
						NorthLost.Invoke();
					}
				}
				else // Tracking is fine
				{
					trackingLost = false;
				}
			}
		}
	}
}
