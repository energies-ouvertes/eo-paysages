// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using EOPaysages.Location;
using SimpleFileBrowser;

namespace EOPaysages.Orientation
{
	///<summary>
	/// Calibrates the North offset by pointing the device at a landmark.
	///</summary>
	public class Landmarks : MonoBehaviour
	{
		[SerializeField]
		Transform poseDriver;


		[Header("Prefab variables")]
		[SerializeField]
		Canvas canvas;

		[SerializeField]
		List<GameObject> hideWhenNoSelection = new List<GameObject>();

		[SerializeField]
		Dropdown landmarkDropdown;

		int currentLandmark = -1;
		public int CurrentLandmark => currentLandmark;

		List<LandmarkEntry> entries = new List<LandmarkEntry>();
		ReadOnlyCollection<LandmarkEntry> Entries => entries.AsReadOnly();

		public readonly UnityEvent ListUpdated = new UnityEvent();

		public void Start()
		{
			NorthManager.Instance.NorthLost.AddListener(onNorthLost);
			NorthManager.Instance.NorthUpdated.AddListener(onNorthUpdated);
			onNorthLost();

			onEntriesListUpdate();
		}

		///<summary>
		/// Set the current landmark, in an inclusive 0 to entries.Count 
		/// range, 0 being none.
		///</summary>
		public void SetLandmarkFromUI(int landmark)
		{
			landmark--; // Remap range so 0 is -1
			if (landmark < 0 || landmark >= entries.Count)
			{
				currentLandmark = -1;
			}
			else
			{
				currentLandmark = landmark;
			}

			foreach (GameObject obj in hideWhenNoSelection)
			{
				obj.SetActive(currentLandmark >= 0);
			}
		}

		public void ShowFileBrowser()
		{
			StartCoroutine(showLoadDialogCoroutine());
		}

		IEnumerator showLoadDialogCoroutine()
		{
			// Set filters (optional)
			FileBrowser.SetFilters(true, new FileBrowser.Filter[]
			{
				new FileBrowser.Filter("Landmarks (.json)", ".json"),
			});

			// Set default filter that is selected when the dialog is shown (optional)
			// Returns true if the default filter is set successfully
			FileBrowser.SetDefaultFilter(".json");

			// Set excluded file extensions (optional) (by default, .lnk and .tmp extensions are excluded)
			// Note that when you use this function, .lnk and .tmp extensions will no longer be
			// excluded unless you explicitly add them as parameters to the function
			FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".rar", ".7z", ".exe", ".zip");


			// Show a load file dialog and wait for a response from user
			// Load file/folder: both, Allow multiple selection: true
			// Initial path: default (Documents), Initial filename: empty
			// Title: "Load File", Submit button text: "Load"
			yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.Files, false, null, null, "Load File", "Load");

			if (FileBrowser.Success)
			{
				foreach (string selectedPath in FileBrowser.Result)
				{
					loadFile(selectedPath);
				}
			}
		}

		void loadFile(String path)
		{
			String data = System.IO.File.ReadAllText(path);
			if (data != null)
			{
				LandmarksList obj = JsonUtility.FromJson<LandmarksList>(data);
				if (obj != null && obj.landmarks != null)
				{
					entries = obj.landmarks;
					onEntriesListUpdate();
					currentLandmark = -1;
					ListUpdated.Invoke();
				}
			}
		}

		void onEntriesListUpdate()
		{
			List<Dropdown.OptionData> options = new List<Dropdown.OptionData>(entries.Count);

			Dropdown.OptionData data = new Dropdown.OptionData();
			data.text = "(Aucun)";
			options.Add(data);

			foreach (LandmarkEntry entry in entries)
			{
				data = new Dropdown.OptionData();
				data.text = entry.Name;

				options.Add(data);
			}
			landmarkDropdown.ClearOptions();
			landmarkDropdown.AddOptions(options);
			landmarkDropdown.value = 0;
			SetLandmarkFromUI(landmarkDropdown.value);
		}

		void onNorthUpdated()
		{
			canvas.gameObject.SetActive(false);
		}

		void onNorthLost()
		{
			canvas.gameObject.SetActive(true);
		}

		public void Calibrate()
		{
			Vector2 gnssVec = ((Geopoint)entries[currentLandmark]).RelativeTo(OriginManager.WorldOrigin).xz();

			float yaw = poseDriver.transform.localRotation.eulerAngles.y;
			yaw *= Mathf.Deg2Rad;
			Vector2 virtualVec = new Vector2(Mathf.Cos(yaw), Mathf.Sin(yaw));

			float angle = Vector2.SignedAngle(virtualVec, gnssVec);

			if (!float.IsNaN(angle))
			{
				Debug.Log($"Landmarks: ARCore vector: {virtualVec}");
				Debug.Log($"Landmarks: GNSS vector:   {gnssVec}");

				NorthManager.Instance.North = angle;
			}
		}


		[Serializable]
		class LandmarksList
		{
			[SerializeField]
			public List<LandmarkEntry> landmarks;
		}

		[Serializable]
		public class LandmarkEntry
		{
			[SerializeField]
			String name;
			public String Name => name;

			[SerializeField]
			double lat;

			[SerializeField]
			double lon;

			public static explicit operator Geopoint(LandmarkEntry e) => new Geopoint(e.lat, e.lon, 0);
		}
	}
}
