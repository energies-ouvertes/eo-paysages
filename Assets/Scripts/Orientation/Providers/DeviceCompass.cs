// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages.Orientation
{
	///<summary>
	/// Uses the device's compass (usually very imprecise) and poseDriver orientation to set North offset.
	///</summary>
	public class DeviceCompass : MonoBehaviour
	{
		void Start()
		{
			Input.compass.enabled = true;
		}

		double lastTimestamp = 0;

		void Update()
		{
			if (Input.compass.timestamp != lastTimestamp)
			{
				updateNorth();
			}
		}

		Vector2 magneticVec = Vector2.up;

		void updateNorth()
		{
			lastTimestamp = Input.compass.timestamp;

			// Fix this
			// TODO: Convert this to geographic North
			Vector3 projected = Vector3.ProjectOnPlane(Input.compass.rawVector * 2, Vector3.up);
			magneticVec.x = projected.x;
			magneticVec.y = projected.z;
			NorthManager.Instance.North = Vector2.SignedAngle(magneticVec, Vector2.up);
		}
	}
}
