// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using EOPaysages.Location;

namespace EOPaysages.Orientation
{
	///<summary>
	/// Uses multiple GNSS readings to set North offset.
	///</summary>
	public class GNSSCompass : MonoBehaviour
	{
		[SerializeField]
		ARPoseDriver poseDriver;

		[Header("Prefab variables")]
		[SerializeField]
		UI.GNSSCompassWindow infoWindow;

		ARSession arSession;

		const double CALIBRATION_DISTANCE_THRESHOLD = 1.0;

		int pointsRequired = 10;

		Coroutine coroutine;

		public enum CalibrationState {NEEDED, ONGOING, OK};
		CalibrationState state = CalibrationState.NEEDED;

		void Awake()
		{
			arSession = FindObjectOfType<ARSession>();
		}

		void Start()
		{
			NorthManager.Instance.NorthLost.AddListener(onNorthLost);
			NorthManager.Instance.NorthUpdated.AddListener(onNorthUpdated);
			onNorthLost();
		}

		void onNorthLost()
		{
			state = CalibrationState.NEEDED;
			infoWindow.UpdateStatus(state, 0, pointsRequired);
		}
		
		void onNorthUpdated()
		{
			state = CalibrationState.OK;
			infoWindow.UpdateStatus(state, 0, pointsRequired);
		}

		public void StartCalibration()
		{
			if (state != CalibrationState.NEEDED)
			{
				return;
			}
			if (coroutine != null)
			{
				StopCalibration();
			}
			coroutine = StartCoroutine(calibrate());
		}

		public void StopCalibration()
		{
			StopCoroutine(coroutine);
			coroutine = null;
			state = CalibrationState.NEEDED;
			infoWindow.UpdateStatus(state, 0, pointsRequired);
		}

		IEnumerator calibrate()
		{
			state = CalibrationState.ONGOING;
			infoWindow.UpdateStatus(state, 0, pointsRequired);
			
			// TODO: Measure precision

			Input.location.Start(.1f, .1f);

			List<Vector2d> geopoints = new List<Vector2d>();
			List<Vector2d> arPoints = new List<Vector2d>();

			// Loop until we have enough points
			while(geopoints.Count < pointsRequired)
			{
				double lastTimestamp = Input.location.lastData.timestamp;
				GNSSReading gnssData;
				Vector2d virtualPosition;
				
				while (true)
				{
					virtualPosition = (Vector2d)poseDriver.transform.localPosition.xz();

					gnssData = (GNSSReading)Input.location.lastData;

					if (gnssData.timestamp > lastTimestamp)
					{
						// "Consume" the new point
						lastTimestamp = gnssData.timestamp;

						// Always accept the first point
						if (arPoints.Count == 0)
						{
							break;
						}
						else if (Vector2d.Distance(arPoints[arPoints.Count-1], virtualPosition) >= CALIBRATION_DISTANCE_THRESHOLD)
						{
							break;
						}
					}

					yield return null;
				}
				
				Debug.Log($"Compass: Calibration point {geopoints.Count} at {virtualPosition}, {gnssData.geopoint}");

				geopoints.Add(new Vector2d(gnssData.geopoint.Longitude, gnssData.geopoint.Latitude));
				arPoints.Add(virtualPosition);

				infoWindow.UpdateStatus(state, geopoints.Count, pointsRequired);
			}

			Vector2 realPoint = getLineVec(geopoints);
			Vector2 virtualPoint = getLineVec(arPoints);

			float angle = Vector2.SignedAngle(realPoint, virtualPoint);
			
			if (!float.IsNaN(angle))
			{
				Debug.Log($"Compass: Calibrated! Correction angle is {angle} degrees");

				{
					Vector3 point = new Vector3((float)arPoints[arPoints.Count-1].x, 0f, (float)arPoints[arPoints.Count-1].y);
					Vector3 pointRotated =  Quaternion.Euler(0, angle, 0) * point;
					Debug.Log($"Compass: Point {point} would now be at {pointRotated}");
				}
				
				NorthManager.Instance.North = angle;

				state = CalibrationState.NEEDED;

				if (Debug.isDebugBuild)
				{
					writeCalibrationDataToFile(geopoints, arPoints, angle, realPoint, virtualPoint);
				}

				UI.Alerts.Instance.Alert("Boussole calibrée");
			}
			else
			{
				UI.Alerts.Instance.Alert("Erreur de calibration");
			}
		}

		static void writeCalibrationDataToFile(List<Vector2d> geopoints, List<Vector2d> arPoints, float angle, Vector2 realLine, Vector2 virtualLine)
		{
			string[] lines = new string[geopoints.Count + 4];

			for (int i=0; i<geopoints.Count; i++)
			{
				lines[i] = $"{i.ToString("D2")}: {arPoints[i].ToString("F5", CultureInfo.InvariantCulture)}  -  {geopoints[i].ToString("F14", CultureInfo.InvariantCulture)}";
			}

			lines[lines.Length - 3] = $"Virtual direction vector: {virtualLine.ToString("F5", CultureInfo.InvariantCulture)}";
			lines[lines.Length - 2] = $"Real direction vector:    {realLine.ToString("F5", CultureInfo.InvariantCulture)}";
			lines[lines.Length - 1] = $"Computed angle = {angle.ToString(CultureInfo.InvariantCulture)}";

			string filename = "north_caliration_data_" + System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".txt";
			System.IO.File.WriteAllLines(System.IO.Path.Combine(Application.persistentDataPath, filename), lines);
		}

		static Vector2 getLineVec(IList<Vector2d> points)
		{
			Debug.Log("Linear best fit");
			Utils.GenerateLinearBestFit(points, out double lineA, out _, out bool lineSwapped);

			// Get a vector parallel to the line
			Vector2 point = new Vector2(1, (float)lineA);
			if (lineSwapped)
			{
				point = point.yx();
			}
			// Make sure the vector is correctly oriented
			Debug.Log("Line direction check");
			Vector2d firstToLast = points[points.Count-1] - points[0];
			if (firstToLast.magnitude <= 0.0001) {
				firstToLast *= 10000.0;
			}
			if (Vector2.Dot(point, (Vector2)firstToLast) < 0)
			{
				point = -point;
			}

			return point;
		}
	}
}
