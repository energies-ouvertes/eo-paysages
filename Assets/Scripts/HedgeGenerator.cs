// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages
{
	///<summary>
	/// Generates a mesh from a list of Geopoints.
	///</summary>
	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(GeoPlacement))]
	public class HedgeGenerator : MonoBehaviour
	{
		List<Geopoint> _geopoints = new List<Geopoint>();
		bool pointsDirty = false;
		public IReadOnlyList<Geopoint> Geopoints
		{
			get
			{
				return _geopoints.AsReadOnly();
			}
			set
			{
				_geopoints = new List<Geopoint>(value);
				pointsDirty = true;
				checkDirtyPoints();
			}
		}

		void OnEnable()
		{
			checkDirtyPoints();
		}

		void checkDirtyPoints()
		{
			if (pointsDirty)
			{
				generate(_geopoints);
			}
		}

		Coroutine generationCoroutine = null;
		void generate(IList<Geopoint> geopoints)
		{
			if (geopoints.Count < 2)
			{
				Debug.LogWarning("Trying to generate an empty hedge");
				return;
			}

			// Abort previous generation routine
			if (generationCoroutine != null)
			{
				StopCoroutine(generationCoroutine);
			}

			generationCoroutine = StartCoroutine("generateCoroutine", geopoints);
		}

		IEnumerator generateCoroutine(IList<Geopoint> geopoints)
		{
			Debug.Log($"Generating hedge mesh with {geopoints.Count} geopoints...");

			GetComponent<GeoPlacement>().Position = geopoints[0];

			// Clone the list since we are going to modify it
			geopoints = new List<Geopoint>(geopoints);

			// Get altitudes
			for (int i=0; i<geopoints.Count; i++)
			{
				yield return geopoints[i].GetElevationCoroutine((valueWithElevation) =>
				{
					geopoints[i] = valueWithElevation;
				});
			}

			// Cartesian points relative to the first point
			List<Vector3> points = new List<Vector3>(geopoints.Count);
			// First point relative to itself is 0,0,0
			points.Add(Vector3.zero);

			const float desiredSubSegmentLength = 5f;

			// Split segments in sub-segments every few meters to better follow the ground
			Vector3 start = points[0];
			for (int i=0; i<geopoints.Count - 1; i++)
			{
				Vector3 end = geopoints[i+1].RelativeTo(geopoints[0]);
				float distance = Vector3.Distance(start, end);

				// Skip the subdividing process when not needed
				if (distance >= desiredSubSegmentLength)
				{
					// Number of sub-segments in this segment
					int divisions = 2;
					while (distance / divisions > desiredSubSegmentLength)
					{
						divisions++;
					}

					float subSegLength = distance / divisions;

					for (int j=0; j<divisions - 1; j++)
					{
						float t = (j+1f)/divisions;

						// TODO: This is using a simple lerp between geopoints, which isn't very accurate
						Geopoint newPoint = new Geopoint( Utils.Lerp(geopoints[i].Latitude, geopoints[i+1].Latitude, t), Utils.Lerp(geopoints[i].Longitude, geopoints[i+1].Longitude, t), 0 );
					
						yield return ARSceneManager.Instance.ElevationProvider.GetElevationAt(newPoint, ((elevation) => 
						{
							newPoint = new Geopoint(newPoint.Latitude, newPoint.Longitude, elevation);
						}));

						points.Add(newPoint.RelativeTo(geopoints[0]));
					}
				}

				// Add segment end point
				points.Add(end);

				start = end;
			}

			// Generate segment normals
			Vector3[] segmentNormals = new Vector3[points.Count - 1];
			for (int i=0; i<segmentNormals.Length; i++)
			{
				Vector3 a = points[i];
				Vector3 b = points[i+1];

				Vector3 segmentDirection = (b - a).normalized;
				// 90 degrees rotation
				segmentNormals[i] = new Vector3( -segmentDirection.z, 0f, segmentDirection.x );
			}

			// Generate mesh

			List<Vector3> vertices = new List<Vector3>(4 * points.Count);
			List<Vector2> uvs = new List<Vector2>(vertices.Capacity);
			List<Color> colors = new List<Color>(vertices.Capacity);
			List<int> indices = new List<int>(18 * segmentNormals.Length + 6);

			float halfThickness = 0.5f;

			float cumulatedUV = 0;

			// Create vertices
			for (int i=0; i<points.Count; i++)
			{
				Vector3 normalA = segmentNormals[i==0 ? 0 : i-1];
				Vector3 normalB = segmentNormals[i>=segmentNormals.Length ? segmentNormals.Length - 1 : i];
				Vector3 normal = (normalA + normalB).normalized;

				Vector3 vertA = points[i] + normal * halfThickness;
				Vector3 vertB = points[i] - normal * halfThickness;

				vertices.Add(vertA);
				vertices.Add(vertA + Vector3.up);
				vertices.Add(vertB);
				vertices.Add(vertB + Vector3.up);

				if (i>0)
				{
					cumulatedUV += Vector3.Distance(points[i-1], points[i]);
				}
				uvs.Add(new Vector2( cumulatedUV, 0 ));
				uvs.Add(new Vector2( cumulatedUV, 1 ));
				uvs.Add(new Vector2( cumulatedUV, halfThickness * 2 ));
				uvs.Add(new Vector2( cumulatedUV, 1 + halfThickness * 2 ));

				// Vertex colors are used to store vertices height from the ground
				colors.Add(Color.black);
				colors.Add(Color.red);
				colors.Add(Color.black);
				colors.Add(Color.red);
			}

			// Create triangles
			int vertex = 0;
			for (int i=0; i<segmentNormals.Length; i++)
			{
				// Start cap
				if (i == 0)
				{
					indices.Add(vertex + 2);
					indices.Add(vertex + 0);
					indices.Add(vertex + 3);
					indices.Add(vertex + 0);
					indices.Add(vertex + 1);
					indices.Add(vertex + 3);
				}
				// End cap
				if (i == segmentNormals.Length - 1)
				{
					indices.Add(vertex + 6);
					indices.Add(vertex + 7);
					indices.Add(vertex + 4);
					indices.Add(vertex + 4);
					indices.Add(vertex + 7);
					indices.Add(vertex + 5);
				}

				// Side 1
				indices.Add(vertex + 0);
				indices.Add(vertex + 4);
				indices.Add(vertex + 1);
				indices.Add(vertex + 4);
				indices.Add(vertex + 5);
				indices.Add(vertex + 1);

				// Side 2
				indices.Add(vertex + 6);
				indices.Add(vertex + 2);
				indices.Add(vertex + 7);
				indices.Add(vertex + 2);
				indices.Add(vertex + 3);
				indices.Add(vertex + 7);

				// Top
				indices.Add(vertex + 1);
				indices.Add(vertex + 5);
				indices.Add(vertex + 3);
				indices.Add(vertex + 5);
				indices.Add(vertex + 7);
				indices.Add(vertex + 3);

				// Move to next point
				vertex += 4;
			}

			// Finalize mesh
			MeshFilter mf = GetComponent<MeshFilter>();
			Mesh mesh = mf.mesh;
			mesh.Clear();
			mesh.SetVertices(vertices);
			mesh.SetUVs(0, uvs);
			mesh.SetColors(colors);
			mesh.SetIndices(indices, MeshTopology.Triangles, 0);
			mesh.RecalculateNormals();
			mesh.RecalculateTangents();
			mesh.RecalculateBounds();


			pointsDirty = false;
		}
	}
}
