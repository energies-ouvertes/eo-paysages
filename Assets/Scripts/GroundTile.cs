// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using EOPaysages.Elevation;
using EOPaysages.Location;

namespace EOPaysages
{
	///<summary>
	/// Parent object for a ground tile.
	///</summary>
	[RequireComponent(typeof(GeoPlacement))]
	public class GroundTile : MonoBehaviour
	{
		[SerializeField]
		TileMeshObject[] meshes;

		ElevationTile _tile;
		public ElevationTile Tile
		{
			get => _tile;
			set
			{
				_tile = value;

				if (_tile.IsValid)
				{
					geoPlacement.Position = Tile.NorthWest;
					Mesh mesh = meshGenerator.GenerateMesh(Tile);
					foreach (TileMeshObject tileMesh in meshes)
					{
						tileMesh.Tile = Tile;
						tileMesh.MeshFilter.sharedMesh = mesh;
					}
				}
				else
				{
					Debug.LogError("Invalid tile?");
				}
			}
		}

		TileMeshGenerator meshGenerator;
		GeoPlacement geoPlacement;

		void Awake()
		{
			geoPlacement = GetComponent<GeoPlacement>();
		}

		///<summary>
		/// Used because Unity cannot pass arguments to component constructors.
		///</summary>
		public void Initialize(TileMeshGenerator meshGenerator)
		{
			this.meshGenerator = meshGenerator;
		}
	}
}
