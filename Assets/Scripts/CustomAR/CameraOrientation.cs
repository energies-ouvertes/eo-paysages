﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages.CustomAR
{
	///<summary>
	/// Handles rotation with gyroscope or mouse look.
	///</summary>
	public class CameraOrientation : MonoBehaviour
	{
		Gyroscope gyro;

		readonly Quaternion correctionQuaternion = Quaternion.Euler(90f, 0f, 0f);

		bool mouseLook = false;
		bool firstMouseLookFrame = true;
		KeyCode mouseLookKey = KeyCode.Space;

		void Awake()
		{
			gyro = Input.gyro;
			gyro.enabled = true;

			if (SystemInfo.deviceType == DeviceType.Desktop)
			{
				mouseLook = true;
			}
		}

		void LateUpdate()
		{
#if UNITY_EDITOR
			if (mouseLook)
			{
				// if we're running on desktop, but the remote connects, use gyro again
				if (UnityEditor.EditorApplication.isRemoteConnected)
				{
					mouseLook = false;
				}

				if (Input.GetKey(mouseLookKey))
				{
					Cursor.lockState = CursorLockMode.Locked;

					float pitchD = - 210 * Input.GetAxis("Mouse Y") * Time.smoothDeltaTime;
					float yawD = 210 * Input.GetAxis("Mouse X") * Time.smoothDeltaTime;

					if (firstMouseLookFrame && pitchD != 0 && yawD != 0)
					{
						firstMouseLookFrame = false;
					}
					else
					{
						// float pitch = transform.eulerAngles.x;
						// pitch = Mathf.Clamp(pitch + pitchD, -75f, 75f);
						// transform.eulerAngles = new Vector3(pitch, transform.eulerAngles.y + yawD, 0);
						transform.Rotate(pitchD, 0, 0);
						transform.Rotate(0, yawD, 0, Space.World);
					}
				}
				else
				{
					Cursor.lockState = CursorLockMode.None;
					firstMouseLookFrame = true;
				}
				return;
			}
			else if (Cursor.lockState == CursorLockMode.Locked)
			{
				Cursor.lockState = CursorLockMode.None;
			}
#endif

			// Fix landscape screen orientation
			transform.rotation = correctionQuaternion * GyroToUnity(gyro.attitude);
		}

		static Quaternion GyroToUnity(Quaternion q)
		{
			return new Quaternion(q.x, q.y, -q.z, -q.w);
		}
	}
}
