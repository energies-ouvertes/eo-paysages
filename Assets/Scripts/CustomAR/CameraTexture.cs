﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages.CustomAR
{
	///<summary>
	/// Starts the default camera and assigns the texture to the current renderer.
	///</summary>
	public class CameraTexture : MonoBehaviour
	{
		public Material SkyMaterial;
		WebCamTexture webcamTexture = null;
		int lastWidth = 0, lastHeight = 0;

		void Start()
		{
			Application.RequestUserAuthorization(UserAuthorization.WebCam);
		}

		void Update()
		{
			if (webcamTexture == null)
			{
				if (WebCamTexture.devices.Length > 0)
				{
					var res = Screen.currentResolution;
					webcamTexture = new WebCamTexture(WebCamTexture.devices[0].name, res.width, res.height, res.refreshRate);
					SkyMaterial.mainTexture = webcamTexture;
					webcamTexture.Play();
				}
			}
			if (webcamTexture != null)
			{
				if (webcamTexture.width != lastWidth || webcamTexture.height != lastHeight)
				{
					SkyMaterial.SetVector("texRes", new Vector2(webcamTexture.width, webcamTexture.height));
					Debug.Log("screen res: "+ Screen.width + "x" + Screen.height);
					Debug.Log("WCT res: "+ webcamTexture.width + "x" + webcamTexture.height);
					lastWidth = webcamTexture.width;
					lastHeight = webcamTexture.height;
				}
			}
		}

		void OnApplicationQuit()
		{
			SkyMaterial.SetVector("texRes", new Vector2(8, 8));
		}
	}
}
