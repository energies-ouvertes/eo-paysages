// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages
{
	///<summary>
	/// Holds references to the main components of the scene. 
	/// There should always be one instance of this in the scene. 
	/// Make sure to fill all the fields in the inspector. 
	///</summary>
	public class ARSceneManager : MonoBehaviour
	{
		static ARSceneManager _instance = null;
		///<summary>
		/// Get the unique singleton instance.
		///</summary>
		public static ARSceneManager Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = FindObjectOfType<ARSceneManager>();
					if (_instance == null)
					{
						Debug.LogError("No ARSceneManager found in scene!");
					}
				}
				return _instance;
			}
		}

		[SerializeField]
		Camera _mainCamera;
		public Camera MainCamera => _mainCamera;

		[SerializeField]
		Elevation.ElevationProvider _elevationProvider;
		public Elevation.ElevationProvider ElevationProvider => _elevationProvider;

		[SerializeField]
		Scenario.ScenariosManager _scenariosManager;
		public Scenario.ScenariosManager ScenariosManager => _scenariosManager;

		void Awake()
		{
			if (MainCamera == null)
			{
				Debug.LogError("ARSceneManager has no MainCamera!");
			}
			if (ElevationProvider == null)
			{
				Debug.LogError("ARSceneManager has no ElevationProvider!");
			}
			if (ScenariosManager == null)
			{
				Debug.LogError("ARSceneManager has no ScenarioManager!");
			}
		}
	}
}
