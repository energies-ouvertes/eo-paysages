﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using System;
using EOPaysages.Location;

// Source: http://guideving.blogspot.com/2010/08/sun-position-in-c.html

namespace EOPaysages
{
	///<summary>
	/// Rotates the object to match the sun's orientation at given a location and date.
	///</summary>
	public class SunOrientation : MonoBehaviour
	{
		private DateTime _date;
		public DateTime Date
		{
			get => _date;
			set
			{
				_date = value;
				updateSunAtOrigin();
			}
		}
		const double Deg2Rad = Math.PI / 180.0;
		const double Rad2Deg = 180.0 / Math.PI;

		void updateSunAtOrigin()
		{
			updateSun(OriginManager.WorldOrigin.Latitude, OriginManager.WorldOrigin.Longitude);
		}

		void updateSun(double latitude, double longitude)
		{
			Debug.Log($"SunOrientation: Setting date to {Date}");

			// Convert to UTC
			DateTime utc = Date.ToUniversalTime();

			// Number of days from J2000.0.
			double julianDate = 367 * utc.Year -
				(int)((7.0 / 4.0) * (utc.Year + 
				(int)((utc.Month + 9.0) / 12.0))) +
				(int)((275.0 * utc.Month) / 9.0) +
				utc.Day - 730531.5;
			
			double julianCenturies = julianDate / 36525.0;

			// Sidereal Time
			double siderealTimeHours = 6.6974 + 2400.0513 * julianCenturies;
			
			double siderealTimeUT = siderealTimeHours +
				(366.2422 / 365.2422) * (double)utc.TimeOfDay.TotalHours;
			
			double siderealTime = siderealTimeUT * 15 + longitude;

			// Refine to number of days (fractional) to specific time.
			julianDate += (double)utc.TimeOfDay.TotalHours / 24.0;
			julianCenturies = julianDate / 36525.0;

			// Solar Coordinates
			double meanLongitude = correctAngle(Deg2Rad *
				(280.466 + 36000.77 * julianCenturies));
			
			double meanAnomaly = correctAngle(Deg2Rad *
				(357.529 + 35999.05 * julianCenturies));
			
			double equationOfCenter = Deg2Rad * ((1.915 - 0.005 * julianCenturies) * 
				Math.Sin(meanAnomaly) + 0.02 * Math.Sin(2 * meanAnomaly));
			
			double elipticalLongitude =
				correctAngle(meanLongitude + equationOfCenter);
			
			double obliquity = (23.439 - 0.013 * julianCenturies) * Deg2Rad;

			// Right Ascension
			double rightAscension = Math.Atan2(
				Math.Cos(obliquity) * Math.Sin(elipticalLongitude),
				Math.Cos(elipticalLongitude)
			);
			
			double declination = Math.Asin( Math.Sin(rightAscension) * Math.Sin(obliquity) );

			// Horizontal Coordinates
			double hourAngle = correctAngle(siderealTime * Deg2Rad) - rightAscension;
			
			if (hourAngle > Math.PI)
			{
				hourAngle -= Utils.PI2;
			}

			double altitude = Math.Asin(Math.Sin(latitude * Deg2Rad) *
				Math.Sin(declination) + Math.Cos(latitude * Deg2Rad) *
				Math.Cos(declination) * Math.Cos(hourAngle));

			// Nominator and denominator for calculating Azimuth
			// angle. Needed to test which quadrant the angle is in.
			double aziNom = -Math.Sin(hourAngle);
			double aziDenom =
				Math.Tan(declination) * Math.Cos(latitude * Deg2Rad) -
				Math.Sin(latitude * Deg2Rad) * Math.Cos(hourAngle);
			
			double azimuth = Math.Atan(aziNom / aziDenom);
			
			if (aziDenom < 0) // In 2nd or 3rd quadrant
			{
				azimuth += Math.PI;
			}
			else if (aziNom < 0) // In 4th quadrant
			{
				azimuth += Utils.PI2;
			}

			// Altitude
			// Debug.Log("Altitude: " + altitude * Rad2Deg);
			transform.rotation = Quaternion.Euler((float)(altitude*Rad2Deg), (float)(azimuth*Rad2Deg), 0.0f);

			// Azimut
			// Debug.Log("Azimuth: " + azimuth * Rad2Deg);
		}

		///<summary>
		/// Corrects an angle.
		///</summary>
		///<returns>
		/// An angle in the range 0 to 2*PI.
		///</returns>
		static double correctAngle(double angleInRadians)
		{
			if (angleInRadians < 0)
			{
				return Utils.PI2 - (Math.Abs(angleInRadians) % (Utils.PI2));
			}
			else if (angleInRadians > Utils.PI2)
			{
				return angleInRadians % (Utils.PI2);
			}
			else
			{
				return angleInRadians;
			}
		}
	}
}
