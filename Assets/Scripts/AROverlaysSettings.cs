﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using UnityEngine.Events;

namespace EOPaysages
{
	///<summary>
	/// Global scene settings events
	///</summary>
	public static class AROverlaysSettings
	{
		///<summary>
		/// Event fired when turbine highlighting is toggled on of off.
		///</summary>
		public static readonly UnityEvent TurbineHighlightChanged = new UnityEvent();
		static bool _turbinesHighlight = false;
		///<summary>
		/// Is turbine highlighting enabled
		///</summary>
		public static bool TurbinesHighlight
		{
			get => _turbinesHighlight;
			set
			{
				_turbinesHighlight = value;
				TurbineHighlightChanged.Invoke();
			}
		}

		public static readonly UnityEvent HedgesVisibilityChanged = new UnityEvent();
		static bool _hedgesVisibility = true;
		public static bool HedgesVisibility
		{
			get => _hedgesVisibility;
			set
			{
				_hedgesVisibility = value;
				HedgesVisibilityChanged.Invoke();
			}
		}

		public static readonly UnityEvent TurbinesRPMChanged = new UnityEvent();
		static float _turbinesRPM = 50;
		public static float TurbinesRPM
		{
			get => _turbinesRPM;
			set
			{
				_turbinesRPM = value;
				TurbinesRPMChanged.Invoke();
			}
		}

		public static readonly UnityEvent WindDirectionChanged = new UnityEvent();
		static float _windDirection = 50;
		public static float WindDirection
		{
			get => _windDirection;
			set
			{
				_windDirection = value;
				WindDirectionChanged.Invoke();
			}
		}

		static float _zonesOpacity = 0.5f;
		public static float ZonesOpacity
		{
			get => _zonesOpacity;
			set
			{
				_zonesOpacity = value;
				refreshZonesOpacity();
			}
		}
		static void refreshZonesOpacity()
		{
			Shader.SetGlobalFloat("GLOBAL_ZONES_OPACITY", ZonesOpacity);
		}

		public static readonly UnityEvent AudioCirclesVisibilityChanged = new UnityEvent();
		static bool _audioCircles = true;
		public static bool AudioCircles
		{
			get => _audioCircles;
			set
			{
				_audioCircles = value;
				AudioCirclesVisibilityChanged.Invoke();
			}
		}

		public static readonly UnityEvent NaturalZonesVisibilityChanged = new UnityEvent();
		static bool _naturalZones = true;
		public static bool NaturalZones
		{
			get => _naturalZones;
			set
			{
				_naturalZones = value;
				NaturalZonesVisibilityChanged.Invoke();
			}
		}

		public static readonly UnityEvent PlatformsVisibilityChanged = new UnityEvent();
		static bool _platforms = true;
		public static bool Platforms
		{
			get => _platforms;
			set
			{
				_platforms = value;
				PlatformsVisibilityChanged.Invoke();
			}
		}

		public static readonly UnityEvent ZIPVisibilityChanged = new UnityEvent();
		static bool _zip = true;
		public static bool ZIPVisibility
		{
			get => _zip;
			set
			{
				_zip = value;
				ZIPVisibilityChanged.Invoke();
			}
		}


		public static readonly UnityEvent DebugModeChanged = new UnityEvent();
		static bool _debugMode = false;
		public static bool DebugMode
		{
			get => _debugMode;
			set
			{
				_debugMode = value;
				DebugModeChanged.Invoke();
			}
		}
	}
}
