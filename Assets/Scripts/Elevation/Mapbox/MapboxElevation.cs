// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using EOPaysages.Location;
using EOPaysages.UI;

namespace EOPaysages.Elevation.Mapbox
{
	///<summary>
	/// Elevation provider from Mapbox ( https://www.mapbox.com ).
	/// This requires an API key provided through the <c>SecretTextsManager</c>.
	///</summary>
	public class MapboxElevation : ElevationProvider
	{
		const int TILE_PIXEL_SIZE = 256;

		const string MAPBOX_SECRET_FILE = "mapbox";

		static string mapboxToken;

		const int SLIPPYMAP_ZOOM_LEVEL = 14;

		static readonly ElevationTileSampler sampler = new MapboxElevationTileSampler();

		Dictionary<(int, int), ElevationTile> tiles = new Dictionary<(int, int), ElevationTile>();

		Dictionary<SlippyMapCoords, Coroutine> currentlyLoading = new Dictionary<SlippyMapCoords, Coroutine>();

		// TODO: Use this to limit dictionary size by unloading distant tiles
		const int MAX_LOADED_TILES = 10;

		void Awake()
		{
			mapboxToken = SecretTextsManager.Get(MAPBOX_SECRET_FILE);
		}

		public override IEnumerator GetTileContaining(Geopoint geopoint, System.Action<ElevationTile> returnValue)
		{
			SlippyMapCoords coords = Utils.GeopointToSlippyMap(geopoint, SLIPPYMAP_ZOOM_LEVEL);

			// TODO: Make sure the coroutine only runs once at a time
			yield return StartCoroutine(getOrLoadTile(coords.X, coords.Y, (tile => {
				returnValue(tile);
			})));
		}

		IEnumerator getOrLoadTile(int x, int y, System.Action<ElevationTile> returnValue)
		{
			(int, int) key = (x, y);
			if (tiles.ContainsKey(key))
			{
				// Debug.Log($"Mapbox elevation: Tile {tiles[key].Coordinates} already loaded");
				returnValue(tiles[key]);
			}
			else
			{
				int zoomLevel = SLIPPYMAP_ZOOM_LEVEL;
				SlippyMapCoords coords = new SlippyMapCoords(x, y, zoomLevel);
				
				if (currentlyLoading.ContainsKey(coords))
				{
					while (currentlyLoading.ContainsKey(coords))
					{
						yield return null;
					}
					returnValue(tiles[key]);
				}
				else
				{
					// Look for cached file
					Option<Texture2D> cached = loadCached(coords);

					if (cached.HasValue)
					{
						ElevationTile tile = new ElevationTile(coords, cached.Value, sampler);
						tiles.Add(key, tile);
						currentlyLoading.Remove(tile.Coordinates);
						returnValue(tile);
						yield break;
					}
					else
					{
						Coroutine coroutine = StartCoroutine(loadTile(coords, (tile => {
							if (!tiles.ContainsKey(key))
							{
								tiles.Add(key, tile);
								currentlyLoading.Remove(tile.Coordinates);
								returnValue(tile);
							}
						})));
						currentlyLoading.Add(coords, coroutine);
						yield return coroutine;
					}
				}
			}
		}

		IEnumerator loadTile(SlippyMapCoords coords, System.Action<ElevationTile> returnValue)
		{
			yield return StartCoroutine(getWebTexture(coords, (texture => {
				returnValue( new ElevationTile(coords, texture, sampler) );
			})));
		}

		static IEnumerator getWebTexture(SlippyMapCoords coords, System.Action<Texture2D> returnValue)
		{
			string uri = getRequestString(coords.X, coords.Y, coords.ZoomLevel);

			Debug.Log($"Mapbox elevation: Web request for {coords}");

			using (UnityWebRequest request = UnityWebRequest.Get(uri))
			{
				yield return request.SendWebRequest();

				Texture2D tileTex = emptyTileTexture();

				int remainingTries = 10;

				while (remainingTries > 0)
				{
					remainingTries--;
					switch (request.result)
					{
						case UnityWebRequest.Result.ConnectionError:
							string msg = $"Mapbox connection error!";
							Debug.LogError(msg);
							Alerts.Instance.Alert(msg);
							break;
						case UnityWebRequest.Result.DataProcessingError:
							msg = $"Mapbox webrequest error for {coords}!";
							Debug.LogError(msg);
							Alerts.Instance.Alert(msg);
							break;
						case UnityWebRequest.Result.ProtocolError:
							// 404 tile not found means it could be only water (0), don't display an error
							if (request.responseCode != 404)
							{
								Debug.LogError($"Mapbox HTTP error for {coords}: {request.responseCode}!\n{request.downloadHandler.text}");
							}
							else
							{
								Debug.Log($"Mapbox returned 404: Tile {coords} is a water tile");
								remainingTries = 0;
							}
							break;
						case UnityWebRequest.Result.Success:
							byte[] data = request.downloadHandler.data;
							tileTex = decodePNG(data);
							saveCached(coords, data);
							remainingTries = 0;
							break;
					}

					if (remainingTries > 0)
					{
						yield return new WaitForSecondsRealtime(1f);
					}
				}

				returnValue(tileTex);
			}
		}

		static Texture2D decodePNG(byte[] data)
		{
			Texture2D tex = new Texture2D(1, 1);
			tex.hideFlags = HideFlags.HideAndDontSave;
			if (ImageConversion.LoadImage(tex, data, false))
			{
				tex.filterMode = FilterMode.Point;
				tex.wrapMode = TextureWrapMode.Clamp;
				return tex;
			}
			else
			{
				Debug.LogError("Could not load elevation tile as texture");
				return emptyTileTexture();
			}
		}

		static Texture2D emptyTileTexture()
		{
			return Texture2D.blackTexture;
		}

		static string getRequestString(int x, int y, int zoomLevel)
		{
			if (mapboxToken == null)
			{
				mapboxToken = SecretTextsManager.Get(MAPBOX_SECRET_FILE);
			}
			return $"https://api.mapbox.com/v4/mapbox.terrain-rgb/{zoomLevel}/{x}/{y}.pngraw?access_token={mapboxToken}";
			// return $"https://api.mapbox.com/v4/mapbox.terrain-rgb/{zoomLevel}/{x}/{y}@2x.pngraw?access_token={mapboxToken}"; // 512*512 resolution
		}

#region cache
		static string _cacheDir = null;
		static string cacheDir
		{
			get
			{
				if (_cacheDir == null)
				{
					_cacheDir = Path.Combine(Application.temporaryCachePath, "mapbox");
					Debug.Log($"Mapbox elevation cache directory: {_cacheDir}");
				}
				return _cacheDir;
			}
		}
		
		static Option<Texture2D> loadCached(SlippyMapCoords coords)
		{
			string path = Path.Combine(cacheDir, cacheFileName(coords));

			if (File.Exists(path))
			{
				Texture2D tex = decodePNG(File.ReadAllBytes(path));
				if (tex != null)
				{
					return Option<Texture2D>.Some(tex);
				}
			}

			return Option<Texture2D>.None;
		}

		static void saveCached(SlippyMapCoords coords, byte[] data)
		{
			string path = Path.Combine(cacheDir, cacheFileName(coords));


			if (File.Exists(path))
			{
				return;
			}

			Directory.CreateDirectory(cacheDir);
			File.WriteAllBytes(path, data);
		}

		static string cacheFileName(SlippyMapCoords coords)
		{
			return $"{coords.ZoomLevel}_{coords.X}_{coords.Y}";
		}
#endregion
	}
}
