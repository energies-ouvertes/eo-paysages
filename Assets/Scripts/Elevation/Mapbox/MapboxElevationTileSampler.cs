// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages.Elevation.Mapbox
{
	///<summary>
	/// Gets elevation data from a Mapbox tile.
	///</summary>
	public class MapboxElevationTileSampler : ElevationTileSampler
	{
		public double GetElevationAtUV(ElevationTile tile, float u, float v)
		{
			// Flip v because texture origin is bottom-left instead of top-left
			Color32 sample = tile.Texture.GetPixelBilinear( u, (1f - v) );
			return convertRGBEncoded(sample);
		}

		///<summary>
		/// Extracts elevation from a mapbox-style RGB tile.
		///</summary>
		///<para name="sample">https://docs.mapbox.com/help/troubleshooting/access-elevation-data/#decode-data</para>
		static double convertRGBEncoded(Color32 sample)
		{
			return -10000 + ((sample.r * 256 * 256 + sample.g * 256 + sample.b) * 0.1);
		}
	}
}
