// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages.Elevation
{
	///<summary>
	/// Interface for retrieving terrain elevation data via coroutines
	///</summary>
	public abstract class ElevationProvider : MonoBehaviour
	{
		///<summary>
		/// Retrieve the ElevationTile containing a given coordinate.
		///</summary>
		public abstract IEnumerator GetTileContaining(Geopoint geopoint, System.Action<ElevationTile> returnValue);

		///<summary>
		/// Retrieve the elevation value at a given coordinate.
		///</summary>
		public IEnumerator GetElevationAt(Geopoint geopoint, System.Action<double> returnValue)
		{
			yield return StartCoroutine(GetTileContaining(geopoint, (tile => {
				returnValue(tile.GetElevationAtGeo(geopoint));
			})));
		}
	}
}
