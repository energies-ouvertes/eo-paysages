// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages.Elevation
{
	///<summary>
	/// Elevation data tile in slippy map coordinates.
	///</summary>
	public readonly struct ElevationTile
	{
		///<summary>An example of invalid tile. Do not use this for comparisons, use IsValid instead.</summary>
		public static readonly ElevationTile INVALID_TILE = new ElevationTile(0, 0, 0, null, new DummyTileSampler());

		///<summary>Check tile's validity.</summary>
		public bool IsValid => Texture != null;

		///<summary>RGB-encoded base 256 elevation value.</summary>
		public readonly Texture2D Texture;

		readonly ElevationTileSampler sampler;

		///<summary>Slippy map coordinates.</summary>
		public readonly SlippyMapCoords Coordinates;

		///<summary>Tile origin point.</summary>
		public readonly Geopoint NorthWest;

		public readonly Geopoint SouthEast;

		///<summary>Tile edge.</summary>
		public double NorthLatitude => NorthWest.Latitude;
		///<summary>Tile edge.</summary>
		public double SouthLatitude => SouthEast.Latitude;
		///<summary>Tile edge.</summary>
		public double EastLongitude => SouthEast.Longitude;
		///<summary>Tile edge.</summary>
		public double WestLongitude => NorthWest.Longitude;

		public ElevationTile(SlippyMapCoords coordinates, Texture2D texture, ElevationTileSampler sampler)
		{
			Coordinates = coordinates;
			Texture = texture;
			this.sampler = sampler;

			NorthWest = Utils.SlippyMapToGeopoint(Coordinates);
			SouthEast = Utils.SlippyMapToGeopoint(Coordinates + Vector2Int.one);
		}

		public ElevationTile(int x, int y, int zoomLevel, Texture2D texture, ElevationTileSampler sampler)
		: this(new SlippyMapCoords(x, y, zoomLevel), texture, sampler)
		{
		}

		///<summary>
		/// Get an elevation value from this tile. 
		/// If the point is not contained in the tile, coordinates are clamped.
		///</summary>
		public double GetElevationAtGeo(Geopoint geopoint)
		{
			// TODO: Test this (seems fine)
			float u = (float) Utils.InverseLerp(WestLongitude, EastLongitude, geopoint.Longitude);
			float v = (float) Utils.InverseLerp(NorthLatitude, SouthLatitude, geopoint.Latitude);

			return GetElevationAtUV(Mathf.Clamp01(u), Mathf.Clamp01(v));
		}

		///<summary>
		/// Samples elevation in normalized tile space, 
		/// with (0.0, 0.0) being the north-west corner.
		///</summary>
		public double GetElevationAtUV(float u, float v)
		{
			return sampler.GetElevationAtUV(this, u, v);
		}

		public override string ToString()
		{
			return base.ToString() + Coordinates.ToString();
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}

			if (!(obj is ElevationTile))
			{
				return false;
			}

			ElevationTile t = (ElevationTile) obj;
			return t.Coordinates == Coordinates && t.Texture == Texture;
		}

		public static bool operator ==(ElevationTile a, ElevationTile b) => a != null && a.Equals(b);

		public static bool operator !=(ElevationTile a, ElevationTile b) => !(a == b);

		public override int GetHashCode() => (Coordinates, Texture).GetHashCode();
	}


	public readonly struct SlippyMapCoords
	{
		public readonly int X;
		public readonly int Y;
		public readonly int ZoomLevel;

		public SlippyMapCoords(int x, int y, int zoomLevel)
		{
			X = x;
			Y = y;
			ZoomLevel = zoomLevel;
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}

			if (!(obj is SlippyMapCoords))
			{
				return false;
			}

			SlippyMapCoords c = (SlippyMapCoords) obj;
			return c.X == X && c.Y == Y && c.ZoomLevel == ZoomLevel;
		}

		public override string ToString() => $"SlippyMap({X}, {Y} @ {ZoomLevel})";
		

		public static SlippyMapCoords operator +(SlippyMapCoords c, Vector2Int v) => new SlippyMapCoords(c.X+v.x, c.Y+v.y, c.ZoomLevel);

		public static SlippyMapCoords operator -(SlippyMapCoords c, Vector2Int v) => c + (-v);

		public static bool operator ==(SlippyMapCoords a, SlippyMapCoords b) => a != null && a.Equals(b);

		public static bool operator !=(SlippyMapCoords a, SlippyMapCoords b) => !(a == b);

		public override int GetHashCode() => (X, Y, ZoomLevel).GetHashCode();
	}

	///<summary>
	/// Dummy sampler used by the INVALID_TILE to prevent errors.
	///</summary>
	class DummyTileSampler : ElevationTileSampler
	{
		public double GetElevationAtUV(ElevationTile tile, float u, float v)
		{
			Debug.LogWarning("Dummy elevation sampler used, 0.0 will be returned!");
			return 0.0;
		}
	}
}
