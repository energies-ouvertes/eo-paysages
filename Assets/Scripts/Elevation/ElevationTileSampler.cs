// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
namespace EOPaysages.Elevation
{
	///<summary>
	/// Generic interface to access implementation-specific elevation data.
	///</summary>
	public interface ElevationTileSampler
	{
		///<summary>
		/// Samples elevation in normalized tile space, 
		/// with (0.0, 0.0) being the north-west corner.
		///</summary>
		public double GetElevationAtUV(ElevationTile tile, float u, float v);
	}
}
