// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages.Elevation
{
	///<summary>
	/// Generates elevation meshes for ElevationTiles.
	/// It is recommended to re-use instances of this class.
	/// This cannot generate meshes in parallel.
	///</summary>
	public class TileMeshGenerator : MonoBehaviour
	{
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uvs = new List<Vector2>();
		List<int> indices = new List<int>();

		public Mesh GenerateMesh(ElevationTile tile)
		{
			generateMeshData(tile, vertices, uvs, indices);

			Mesh mesh = new Mesh();
			mesh.name = tile.Coordinates.ToString();
			mesh.Clear();
			mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
			mesh.SetVertices(vertices);
			mesh.SetUVs(0, uvs);
			mesh.SetIndices(indices, MeshTopology.Triangles, 0);
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();
			return mesh;
		}

		void generateMeshData(ElevationTile tile, List<Vector3> vertices, List<Vector2> uvs, List<int> indices)
		{
			float sizeRatio = (float) tile.Texture.height / tile.Texture.width;
			int width = 256;
			int height = (int)(width * sizeRatio);

			vertices.Clear();
			vertices.Capacity = (width+1)*(height+1);
			
			uvs.Clear();
			uvs.Capacity = (width+1)*(height+1);

			indices.Clear();
			indices.Capacity = width*height*6;

			float widthMeters, heightMeters;
			{
				Vector3 size = tile.SouthEast.RelativeTo(tile.NorthWest);
				widthMeters = size.x;
				heightMeters = size.z;
			}

			Debug.Log($"Ground tile: Generating {vertices.Capacity} vertices");

			for (int y=0; y <= height; y++)
			{
				for (int x=0; x <= width; x++)
				{
					float u = (float)x/width;
					float v = (float)y/height;
					double altitude = tile.GetElevationAtUV(u, v);

					Geopoint point = new Geopoint(
						Utils.Lerp(tile.NorthLatitude, tile.SouthLatitude, v),
						Utils.Lerp(tile.WestLongitude, tile.EastLongitude, u),
						altitude
					);
					vertices.Add( point.RelativeTo(tile.NorthWest) );
					// Simpler but imprecise
					// vertices.Add( new Vector3(u * widthMeters, (float)altitude, v * heightMeters) );

					uvs.Add(new Vector2(u, v));

					if (x < width && y < height)
					{
						int firstIndex = Utils.Vec2iToIndex(x, y, width+1);
						indices.Add(firstIndex);             // 0
						indices.Add(firstIndex + 1);         // 1
						indices.Add(firstIndex + width + 1); // 2

						indices.Add(firstIndex + 1);         // 3
						indices.Add(firstIndex + width + 2); // 4
						indices.Add(firstIndex + width + 1); // 5
					}
				}
			}
		}
	}
}
