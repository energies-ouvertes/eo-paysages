// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using EOPaysages.Location;

namespace EOPaysages.Scenario
{
	///<summary>
	/// Holds data describing a scenario.
	///</summary>
	public readonly struct ScenarioDescriptor
	{
		public readonly string title;

		public readonly Color color;

		public readonly ReadOnlyCollection<Geopoint> turbines;

		public readonly TurbineDimensions turbineDimensions;

		public readonly ReadOnlyCollection<Geopoint[]> hedges;

		public ScenarioDescriptor(string title, Color color, IList<Geopoint> turbines, TurbineDimensions turbineDimensions, IList<IList<Geopoint>> hedges)
		{
			this.title = title;
			
			this.color = color;

			Geopoint[] turbinesArray = new Geopoint[turbines.Count];
			turbines.CopyTo(turbinesArray, 0);
			this.turbines = new ReadOnlyCollection<Geopoint>(turbinesArray);

			this.turbineDimensions = turbineDimensions;
			
			Geopoint[][] hedgesArray = new Geopoint[hedges.Count][];
			hedges.CopyTo(hedgesArray, 0);
			this.hedges = new ReadOnlyCollection<Geopoint[]>(hedgesArray);
		}


		public override int GetHashCode()
		{
			return title.GetHashCode()
			       + 17 * turbines.GetHashCode()
				  	 + 41 * turbineDimensions.GetHashCode();
		}
	}
}