// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using EOPaysages.Location;

namespace EOPaysages.Scenario
{
	///<summary>
	/// Transforms JSON files and strings into <c>ScenarioDescriptor</c> objects.
	///</summary>
	public class ScenarioDescriptorParser
	{
		///<summary>
		/// Attempts to load a scenario from a file path.
		/// This expects a single scenario file, not a directory or an archive.
		///</summary>
		public static Option<ScenarioDescriptor> LoadScenario(string descriptorFilePath)
		{
			Debug.Log($"ScenarioParser: Loading {descriptorFilePath}");

			try
			{
				string text = System.IO.File.ReadAllText(descriptorFilePath);
				string baseDir = new FileInfo(descriptorFilePath).Directory.FullName;
				return LoadScenarioFromData(baseDir, text);
			}
			catch (Exception e)
			{
				Debug.LogError($"ScenarioParser: Failed to load {descriptorFilePath}");
				Debug.LogException(e);
				UI.Alerts.Instance.Alert("Erreur de chargement");
				return Option<ScenarioDescriptor>.None;
			}
		}

		///<summary>
		/// Attempts to load a scenario from its text data.
		///</summary>
		///<param name="baseFolder">
		/// Directory that contained the scenario file.
		/// This is required to load external files referenced in the scenario.
		///</param>
		///<param name="data">
		/// Content of the scenario file.
		///</param>
		public static Option<ScenarioDescriptor> LoadScenarioFromData(string baseFolder, string data)
		{
			JSONObject root = JSON.Parse(data).AsObject;

			Option<String> titleO = Option<String>.None;
			Option<Color> colorO = Option<Color>.None;
			Option<Geopoint[]> turbinesO = Option<Geopoint[]>.None;
			Option<TurbineDimensions> turbineDimensionsO = Option<TurbineDimensions>.None;
			Option<Geopoint[][]> hedgesO = Option<Geopoint[][]>.None;

			foreach (KeyValuePair<string, JSONNode> entry in root)
			{
				switch (entry.Key)
				{
					case "formatVersion":
						break;
					case "title":
						if (entry.Value.IsString)
						{
							titleO = ((JSONString)entry.Value).Value;
						}
						break;
					case "color":
						Color c;
						if (entry.Value.IsString && ColorUtility.TryParseHtmlString(((JSONString)entry.Value).Value, out c))
						{
							// Discard opacity
							colorO = new Color(c.r, c.g, c.b);
						}
						break;
					case "turbinePositions":
						turbinesO = parseGeopointArray(entry.Value);
						break;
					case "turbineDimensions":
						turbineDimensionsO = parseTurbineDimensions(entry.Value);
						break;
					case "hedges":
						hedgesO = parseHedges(entry.Value);
						break;
					default:
						Debug.LogWarning($"ScenarioParser: Unknown entry: {entry.Key}");
						break;
				}
			}

			if (!titleO.HasValue) {
				Debug.LogError("ScenarioParser: Missing title entry");
				UI.Alerts.Instance.Alert("Erreur: Le scénario n'a pas de titre");
				return Option<ScenarioDescriptor>.None;
			}

			if (!colorO.HasValue) {
				Debug.LogError("ScenarioParser: Missing color entry");
				UI.Alerts.Instance.Alert("Erreur: Le scénario n'a pas de couleur");
				return Option<ScenarioDescriptor>.None;
			}

			if (!turbinesO.HasValue) {
				Debug.LogError("ScenarioParser: Missing turbines positions entry");
				UI.Alerts.Instance.Alert($"Erreur: Pas d'éoliennes dans \"{titleO.Value}\"");
				return Option<ScenarioDescriptor>.None;
			}

			TurbineDimensions turbineDimensions = turbineDimensionsO.ValueOrDefault(TurbineDimensions.defaultDimensions);
			
			Geopoint[][] hedges = hedgesO.ValueOrDefault(new Geopoint[][]{});

			Debug.Log($"ScenarioParser: Scenario \"{titleO.Value}\": {turbinesO.Value.Length} turbines, {hedges.Length} hedges.");

			return new ScenarioDescriptor(titleO.Value, colorO.Value, turbinesO.Value, turbineDimensions, hedges);
		}

		///<summary>
		/// Reads an array of hedges.
		/// If any element is not a geopoint array, returns Option.None.
		///</summary>
		static Option<Geopoint[][]> parseHedges(JSONNode node)
		{
			if (!node.IsArray)
			{
				return Option<Geopoint[][]>.None;
			}
			JSONArray arr = node.AsArray;
			Geopoint[][] res = new Geopoint[arr.Count][];
			
			int i = 0;
			foreach (JSONNode pointNode in arr)
			{
				Option<Geopoint[]> point = parseGeopointArray(pointNode);
				if (!point.HasValue)
				{
					return Option<Geopoint[][]>.None;
				}
				res[i] = point.Value;
				i++;
			}

			return res;
		}

		///<summary>
		/// Reads an array of geopoints.
		/// If any element is not a geopoint, returns Option.None.
		///</summary>
		static Option<Geopoint[]> parseGeopointArray(JSONNode node)
		{
			if (!node.IsArray)
			{
				return Option<Geopoint[]>.None;
			}
			JSONArray arr = node.AsArray;
			Geopoint[] res = new Geopoint[arr.Count];
			
			int i = 0;
			foreach (JSONNode pointNode in arr)
			{
				Option<Geopoint> point = parseGeopoint(pointNode);
				if (!point.HasValue)
				{
					return Option<Geopoint[]>.None;
				}
				res[i] = point.Value;
				i++;
			}

			return res;
		}

		static Option<Geopoint> parseGeopoint(JSONNode node)
		{
			if (!node.IsObject)
			{
				return Option<Geopoint>.None;
			}
			JSONObject obj = node.AsObject;
			
			if (!obj.HasKey("lat") || !obj.HasKey("lon"))
			{
				return Option<Geopoint>.None;
			}

			if (obj["lat"].IsNumber && obj["lon"].IsNumber)
			{
				return new Geopoint( obj["lat"].AsDouble, obj["lon"].AsDouble, 0.0 );
			}

			return Option<Geopoint>.None;
		}

		static Option<TurbineDimensions> parseTurbineDimensions(JSONNode node)
		{
			if (!node.IsObject)
			{
				Debug.LogError("ScenarioParser: Node is not a JSON object");
				return Option<TurbineDimensions>.None;
			}
			JSONObject obj = node.AsObject;

			Option<float> heightO = Option<float>.None;
			Option<float> diameterO = Option<float>.None;
			Option<float> rotorDiameterO = Option<float>.None;

			foreach (KeyValuePair<String, JSONNode> entry in obj)
			{
				switch (entry.Key)
				{
					case "height":
						if (entry.Value.IsNumber)
						{
							heightO = entry.Value.AsFloat;
						}
						break;
					case "diameter":
						if (entry.Value.IsNumber)
						{
							diameterO = entry.Value.AsFloat;
						}
						break;
					case "rotorDiameter":
						if (entry.Value.IsNumber)
						{
							rotorDiameterO = entry.Value.AsFloat;
						}
						break;
					default:
						Debug.LogWarning($"ScenarioParser: Unknown entry in turbine dimensions: {entry.Key}");
						break;
				}
			}

			if (heightO.HasValue && diameterO.HasValue && rotorDiameterO.HasValue)
			{
				return new TurbineDimensions(heightO.Value, diameterO.Value, rotorDiameterO.Value);
			}

			// Errors
			
			if (!heightO.HasValue)
			{
				Debug.LogError("ScenarioParser: Missing \"height\" in turbine dimensions");
			}
			if (!diameterO.HasValue)
			{
				Debug.LogError("ScenarioParser: Missing \"diameter\" in turbine dimensions");
			}
			if (!rotorDiameterO.HasValue)
			{
				Debug.LogError("ScenarioParser: Missing \"rotorDiameter\" in turbine dimensions");
			}

			Debug.LogError($"ScenarioParser: Failed to parse turbine dimensions from {node.ToString()}");
			UI.Alerts.Instance.Alert("Gabarit d'éolienne invalide");

			return Option<TurbineDimensions>.None;
		}
	}
}
