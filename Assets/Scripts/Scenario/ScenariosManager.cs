// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EOPaysages.Scenario
{
	///<summary>
	/// Handles loaded scenarios as <c>ScenarioInstance</c>.
	///</summary>
	public class ScenariosManager : MonoBehaviour
	{
		Dictionary<string, ScenarioInstance> instances = new Dictionary<string, ScenarioInstance>();

		public IEnumerable<string> LoadedScenarios => instances.Keys;

#region Prefab parameters
		[Header("Internal Prefab variables")]
		[SerializeField]
		ScenarioInstance instancePrefab;
#endregion

		public readonly UnityEvent ScenarioListChanged = new UnityEvent();

		///<summary>
		/// Retreive a loaded scenario.
		///</summary>
		public Option<ScenarioDescriptor> GetScenario(string descriptorPath)
		{
			if (instances.ContainsKey(descriptorPath))
			{
				return instances[descriptorPath].Scenario;
			}
			return Option<ScenarioDescriptor>.None;
		}

		public void LoadScenarioFile(string descriptorPath)
		{
			Option<ScenarioDescriptor> scenario = ScenarioDescriptorParser.LoadScenario(descriptorPath);
			if (scenario.HasValue)
			{
				LoadScenario(descriptorPath, scenario.Value);
			}
			else
			{
				Debug.LogError($"ScenarioManager: Could not instantiate scenario {descriptorPath}");
			}
		}

		public void LoadScenario(string path, ScenarioDescriptor scenario)
		{
			Debug.Log($"ScenarioManager: Instancing scenario \"{scenario.title}\"");
			
			if (instances.ContainsKey(path))
			{
				Debug.LogWarning($"ScenarioManager: Scenario already instanced: {path}");
				return;
			}

			ScenarioInstance newInstance = Instantiate<ScenarioInstance>(instancePrefab);
			newInstance.gameObject.name = $"ScenarioInstance {scenario.title}";
			newInstance.transform.parent = transform;
			newInstance.Scenario = scenario;
			instances.Add(path, newInstance);

			DisableScenario(path);

			ScenarioListChanged.Invoke();
		}

		public Option<bool> ScenarioVisibility(string path)
		{
			if (instances.ContainsKey(path))
			{
				return Option<bool>.Some(instances[path].gameObject.activeSelf);
			}
			return Option<bool>.None;
		}

		public void EnableScenario(string path)
		{
			setScenarioVisibility(path, true);
		}

		public void DisableScenario(string path)
		{
			setScenarioVisibility(path, false);
		}

		public void DisableAllScenarios()
		{
			foreach (string path in instances.Keys)
			{
				DisableScenario(path);
			}
		}

		void setScenarioVisibility(string path, bool visible)
		{
			if (!instances.ContainsKey(path))
			{
				Debug.LogError($"ScenarioManager: Scenario isn't loaded: {path}");
				return;
			}

			instances[path].gameObject.SetActive(visible);
		}
	}
}
