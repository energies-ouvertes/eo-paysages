// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages
{
	[RequireComponent(typeof(MeshRenderer))]
	public class TurbineHighlightListener : MonoBehaviour
	{
		static Material _highlightMaterial;
		static Material highlightMaterial
		{
			get
			{
				if (_highlightMaterial == null)
				{
					_highlightMaterial = Resources.Load<Material>("Turbine_Highlight");
				}
				return _highlightMaterial;
			}
		}

		Color _color;
		public Color color
		{
			get => _color;
			set
			{
				_color = value;
				refreshMaterials();
			}
		}

		MaterialPropertyBlock mpb;
		int highlightColorId = Shader.PropertyToID("Color_Main");

		Material[] originalMaterials;

		MeshRenderer meshRenderer;

		void Awake()
		{
			meshRenderer = GetComponent<MeshRenderer>();

			originalMaterials = meshRenderer.sharedMaterials;

			mpb = new MaterialPropertyBlock();
		}

		void OnEnable()
		{
			AROverlaysSettings.TurbineHighlightChanged.AddListener(refreshMaterials);
			refreshMaterials();
		}

		void OnDisable()
		{
			AROverlaysSettings.TurbineHighlightChanged.RemoveListener(refreshMaterials);
		}

		void refreshMaterials()
		{
			if (AROverlaysSettings.TurbinesHighlight)
			{
				meshRenderer.sharedMaterials = new Material[originalMaterials.Length].Populate(highlightMaterial);
				mpb.SetColor(highlightColorId, color);
				meshRenderer.SetPropertyBlock(mpb);
			}
			else
			{
				meshRenderer.sharedMaterials = originalMaterials;
				meshRenderer.SetPropertyBlock(null);
			}
		}
	}
}
