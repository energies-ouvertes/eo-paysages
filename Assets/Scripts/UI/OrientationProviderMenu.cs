// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EOPaysages.Orientation;

namespace EOPaysages.UI
{
	///<summary>
	/// Dropdown menu asking the user to chose an orientation provider.
	///</summary>
	public class OrientationProviderMenu : MonoBehaviour
	{
		[SerializeField]
		Landmarks providerLandmarks;

		[SerializeField]
		GNSSCompass providerGNSSCompass;


		enum ProviderType {GNSS, LANDMARKS}
		List<ProviderType> availableProviders;


		[Header("Prefab variables")]
		[SerializeField]
		Dropdown dropdown;

		void OnEnable()
		{
			availableProviders = new List<ProviderType>();
			if (providerGNSSCompass)
			{
				availableProviders.Add(ProviderType.GNSS);
			}
			if (providerLandmarks)
			{
				availableProviders.Add(ProviderType.LANDMARKS);
			}

			
			List<Dropdown.OptionData> options = new List<Dropdown.OptionData>(availableProviders.Count);

			Dropdown.OptionData data = new Dropdown.OptionData();
			data.text = "(Aucun)";
			options.Add(data);

			foreach (ProviderType type in availableProviders)
			{
				data = new Dropdown.OptionData();

				switch (type) {
					case ProviderType.GNSS:
						data.text = "Par déplacement GPS";
						break;
					case ProviderType.LANDMARKS:
						data.text = "Par point de repère";
						break;
				}

				options.Add(data);
			}
			dropdown.ClearOptions();
			dropdown.AddOptions(options);
			dropdown.value = 0;
			OnValueChange(dropdown.value);
		}

		public void OnValueChange(int value)
		{
			if (value == 0) {
				gameObject.SetActive(true);
			}
			else
			{
				gameObject.SetActive(false);
			}

			Debug.Log("Orientation provider changed");
			for (int i=0; i < availableProviders.Count; i++)
			{
				bool enabled = i == value - 1;
				switch(availableProviders[i])
				{
					case ProviderType.GNSS:
						providerGNSSCompass.gameObject.SetActive(enabled);
						break;
					case ProviderType.LANDMARKS:
						providerLandmarks.gameObject.SetActive(enabled);
						break;
				}
			}
		}
	}
}
