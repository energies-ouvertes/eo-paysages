// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using UnityEngine.UI;

namespace EOPaysages.UI
{
	///<summary>
	/// Message displayed by <c>Alerts</c>.
	///</summary>
	[RequireComponent(typeof(RectTransform))]
	[RequireComponent(typeof(Text))]
	public class Alert : MonoBehaviour
	{
		[SerializeField]
		float immobileDuration = 1.0f;
		[SerializeField]
		float speed = 5.0f;
		[SerializeField]
		float margin = 20.0f;

		float immobileTimer = 0f;
		float blendFactor = 0f;

		RectTransform rect;
		Text uiText;
		static readonly Color full = new Color(1, 1, 1, 1);
		static readonly Color hidden = new Color(1, 1, 1, 0);

		void Awake()
		{
			rect = GetComponent<RectTransform>();
			uiText = GetComponent<Text>();
		}

		public void SetupAlert(string text, float startY, Option<Alert> last)
		{
			uiText.text = text;
			immobileTimer = 0f;
			blendFactor = 0f;

			float yMin = startY;
			if (last.HasValue)
			{
				yMin = last.Value.rect.anchoredPosition.y - last.Value.rect.rect.height;
			}

			rect.anchoredPosition = new Vector2(margin, yMin - margin);
		}

		void Update()
		{
			if (immobileTimer < immobileDuration)
			{
				immobileTimer += Time.unscaledDeltaTime;
			}
			else
			{
				if (rect.anchoredPosition.y > 0.0f)
				{
					blendFactor = Mathf.MoveTowards(blendFactor, 1, 0.1f * Time.unscaledDeltaTime);
					uiText.color = Color.Lerp(full, hidden, blendFactor);
					if (blendFactor > 0.999f)
					{
						Destroy(gameObject);
					}
				}
				rect.anchoredPosition += new Vector2(0f, speed * Time.unscaledDeltaTime);
			}
		}
	}
}
