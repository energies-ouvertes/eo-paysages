// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages.UI
{
	///<summary>
	/// Displays messages on the screen that fade away after a few seconds.
	///</summary>
	[RequireComponent(typeof(Canvas))]
	public class Alerts : MonoBehaviour
	{
		static Alerts _instance;
		public static Alerts Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = FindObjectOfType<Alerts>();
					if (_instance == null)
					{
						Debug.LogError("Missing Alerts instance");
					}
				}
				return _instance;
			}
		}


		[SerializeField]
		Alert alertPrefab;

		
		Alert lastAlert = null;
		float canvasTop;

		void Start()
		{
			canvasTop = GetComponent<Canvas>().pixelRect.height;
		}

		public void Alert(string text)
		{
			Alert newAlert = Instantiate<Alert>(alertPrefab);
			Option<Alert> lastOption = Option<Alert>.None;
			if (lastAlert != null)
			{
				lastOption = Option<Alert>.Some(lastAlert);
			}
			newAlert.transform.SetParent(transform, false);
			newAlert.SetupAlert(text, canvasTop, lastOption);
			lastAlert = newAlert;
		}
	}
}
