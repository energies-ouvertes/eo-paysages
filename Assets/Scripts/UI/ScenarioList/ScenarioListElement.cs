// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using UnityEngine.UI;
using EOPaysages.Scenario;

namespace EOPaysages.UI.ScenarioList
{
	///<summary>
	/// Checkbox controlling a scenario's visibility.
	///</summary>
	public class ScenarioListElement : MonoBehaviour
	{
#region Prefab parameters
			[Header("Internal Prefab variables")]

			[SerializeField]
			Toggle toggle;
			
			[SerializeField]
			Text label;
			
			[SerializeField]
			Image scenarioColorIndicator;
#endregion


		string path;

		public void Set(string path, string name)
		{
			this.path = path;
			label.text = name;

			Option<ScenarioDescriptor> scenario = ARSceneManager.Instance.ScenariosManager.GetScenario(path);
			if (!scenario.HasValue)
			{
				Debug.LogError($"ScenarioListElement: Element set to a non-existant scenario: {path}");
			}
			else
			{
				scenarioColorIndicator.color = scenario.Value.color;
			}

			toggle.isOn = ARSceneManager.Instance.ScenariosManager.ScenarioVisibility(path).ValueOrDefault(false);
		}

		public void OnToggle()
		{
			if (path == null || path.Length == 0)
			{
				return;
			}

			if (toggle.isOn)
			{
				ARSceneManager.Instance.ScenariosManager.EnableScenario(path);			
			}
			else
			{
				ARSceneManager.Instance.ScenariosManager.DisableScenario(path);			
			}
		}
	}
}
