// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using UnityEngine;

namespace EOPaysages.UI.ScenarioList
{
	public class ScenarioListPanel : Panel
	{
#region Prefab parameters
		[Header("Internal Prefab variables")]
		[SerializeField]
		ScenarioListElement listElementPrefab;

		[SerializeField]
		RectTransform list;
#endregion

		bool _panelOpen = false;
		public override bool PanelOpen
		{
			get => _panelOpen;
			set
			{
				_panelOpen = value;
				if (value)
				{
					foreach (Panel panel in allPanels)
					{
						if (panel != this)
						{
							panel.PanelOpen = false;
						}
					}
				}
			}
		}

		Dictionary<string, ScenarioListElement> buttons = new Dictionary<string, ScenarioListElement>();

		new void Awake()
		{
			base.Awake();
			PanelOpen = false;
		}

		void Start()
		{
			ARSceneManager.Instance.ScenariosManager.ScenarioListChanged.AddListener(refreshList);
			refreshList();
		}

		void Update()
		{
			transform.localScale = new Vector3(1, Mathf.Lerp(transform.localScale.y, PanelOpen ? 1f : 0f, 20f * Time.deltaTime), 1);

			// Close on escape / android back button
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				PanelOpen = false;
			}
		}

		void refreshList()
		{
			// Remove old elements
			HashSet<string> toRemove = new HashSet<string>();
			foreach (string elem in buttons.Keys)
			{
				if (!ARSceneManager.Instance.ScenariosManager.GetScenario(elem).HasValue)
				{
					toRemove.Add(elem);
				}
			}
			foreach (string elem in toRemove)
			{
				Destroy(buttons[elem].gameObject);
				buttons.Remove(elem);
			}

			// Add new ones
			foreach (string path in ARSceneManager.Instance.ScenariosManager.LoadedScenarios)
			{
				if (!buttons.ContainsKey(path)) {
					ScenarioListElement elem = Instantiate<ScenarioListElement>(listElementPrefab);
					elem.Set(path, ARSceneManager.Instance.ScenariosManager.GetScenario(path).Value.title);
					elem.transform.SetParent(list, false);
					buttons.Add(path, elem);
				}
			}
		}
	}
}
