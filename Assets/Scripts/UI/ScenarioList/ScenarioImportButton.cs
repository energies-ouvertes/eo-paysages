﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using System.IO;
using System.IO.Compression;
using UnityEngine;
using UnityEngine.UI;
using SimpleFileBrowser;
using EOPaysages.Scenario;

namespace EOPaysages.UI.ScenarioList
{
	public class ScenarioImportButton : MonoBehaviour
	{
		void Start()
		{
			GetComponent<Button>().onClick.AddListener(ShowLoadDialog);
		}

		void ShowLoadDialog()
		{
			StartCoroutine(showLoadDialogCoroutine());
		}

		IEnumerator showLoadDialogCoroutine()
		{
			// Set filters (optional)
			FileBrowser.SetFilters(true, new FileBrowser.Filter[]
			{
				new FileBrowser.Filter("Scenario descriptor (.json)", ".json"),
				new FileBrowser.Filter("Zipped scenarios (.zip)", ".zip"),
			});

			// Set default filter that is selected when the dialog is shown (optional)
			// Returns true if the default filter is set successfully
			FileBrowser.SetDefaultFilter(".zip");

			// Set excluded file extensions (optional) (by default, .lnk and .tmp extensions are excluded)
			// Note that when you use this function, .lnk and .tmp extensions will no longer be
			// excluded unless you explicitly add them as parameters to the function
			FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".rar", ".7z", ".exe");


			// Show a load file dialog and wait for a response from user
			// Load file/folder: both, Allow multiple selection: true
			// Initial path: default (Documents), Initial filename: empty
			// Title: "Load File", Submit button text: "Load"
			yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.FilesAndFolders, true, null, null, "Load Files and Folders", "Load");

			if (FileBrowser.Success)
			{
				foreach (string selectedPath in FileBrowser.Result)
				{
					if (FileBrowserHelpers.IsDirectory(selectedPath))
					{
						loadDirectory(selectedPath);
					}
					else
					{
						loadSelectionEntry(selectedPath);
					}
				}
			}
		}

		///<summary>
		/// Loads a directory of scenarios.
		///</summary>
		void loadDirectory(string path)
		{
			foreach (FileSystemEntry entry in FileBrowserHelpers.GetEntriesInDirectory(path))
			{
				string scenarioPath = entry.Path;
				if (!FileBrowserHelpers.IsDirectory(scenarioPath))
				{
					loadSelectionEntry(scenarioPath);
				}
			}
		}

		///<summary>
		/// Loads a single file: either a scenario or a zip.
		///</summary>
		void loadSelectionEntry(string path)
		{
			string extension = Path.GetExtension(path).ToLowerInvariant();

			switch (extension)
			{
				case ".zip":
				{
					// Extract the zip to the cache
					string extractPathParent = Path.Combine(Application.temporaryCachePath, "zip");
					string extractPath = Path.Combine(extractPathParent, path.GetHashCode().ToString());
					if (!Directory.Exists(extractPathParent))
					{
						Directory.CreateDirectory(extractPathParent);
					}
					if (Directory.Exists(extractPath))
					{
						Directory.Delete(extractPath, true);
					}
					
					ZipFile.ExtractToDirectory(path, extractPath);

					loadDirectory(extractPath);

					break;
				}
				case ".json":
				{
					Option<ScenarioDescriptor> scenario = ScenarioDescriptorParser.LoadScenario(path);
					if (scenario.HasValue)
					{
						ARSceneManager.Instance.ScenariosManager.LoadScenario(path, scenario.Value);
					}
					break;
				}
				default:
				{
					Debug.LogError($"Unsupported file extension: \"{extension}\"");
					break;
				}
			}
		}
	}
}
