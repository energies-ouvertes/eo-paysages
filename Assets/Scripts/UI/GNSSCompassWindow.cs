// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using UnityEngine.UI;
using EOPaysages.Orientation;

namespace EOPaysages.UI
{
	///<summary>
	/// Displays <c>GNSSCompass</c> calibration prompt and status in a window.
	///</summary>
	public class GNSSCompassWindow : MonoBehaviour
	{
		[SerializeField]
		Text text;

		[SerializeField]
		Button buttonStart;

		[SerializeField]
		Button buttonCancel;

		void Start()
		{
			gameObject.SetActive(false);
		}

		public void UpdateStatus(GNSSCompass.CalibrationState state, int points, int maxPoints)
		{
			switch (state)
			{
				case GNSSCompass.CalibrationState.ONGOING:
					gameObject.SetActive(true);
					buttonStart.interactable = false;
					buttonCancel.interactable = true;

					text.text = $"Calibration en cours, marchez en ligne droite... (point {points}/{maxPoints})";
					break;
				case GNSSCompass.CalibrationState.NEEDED:
					gameObject.SetActive(true);
					buttonStart.interactable = true;
					buttonCancel.interactable = false;

					text.text = "Direction du Nord inconnue, la boussole doit être calibrée.";
					break;
				case GNSSCompass.CalibrationState.OK:
					gameObject.SetActive(false);
					break;
			}
		}
	}
}
