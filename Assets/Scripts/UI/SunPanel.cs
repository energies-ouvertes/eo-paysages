﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;
using UnityEngine;
using UnityEngine.UI;

namespace EOPaysages.UI
{
	public class SunPanel : Panel
	{
		DateTime dt;

		SunOrientation sun;


		[Header("Internal Prefab variables")]
		[SerializeField]
		Slider sliderDayOfYear;

		[SerializeField]
		Slider sliderMinuteOfDay;

		[SerializeField]
		Text textTime;

		[SerializeField]
		Text textDate;

		string[] months = new string[]
		{
			"Jan.",
			"Fév.",
			"Mars",
			"Avr.",
			"Mai",
			"Juin",
			"Juil.",
			"Août",
			"Sep.",
			"Oct.",
			"Nov.",
			"Déc."
		};

		new void Awake()
		{
			base.Awake();

			dt = DateTime.Now;

			// Find a Sun in the scene
			sun = FindObjectOfType<SunOrientation>();
			if (sun == null)
			{
				Debug.LogError("SunPanel: Cannot find SunOrientation object");
			}
		}

		void Start()
		{
			PanelOpen = false;

			// Init values
			sliderDayOfYear.value = dt.DayOfYear;
			sliderMinuteOfDay.value = dt.Minute + dt.Hour * 60 + 1;

			sun.Date = dt;
		}

		bool _panelOpen = false;
		public override bool PanelOpen
		{
			get => _panelOpen;
			set
			{
				_panelOpen = value;
				if (value)
				{
					foreach (Panel panel in allPanels)
					{
						if (panel != this)
						{
							panel.PanelOpen = false;
						}
					}
				}
			}
		}

		void Update()
		{
			transform.localScale = new Vector3(1, Mathf.Lerp(transform.localScale.y, PanelOpen ? 1f : 0f, 20f * Time.deltaTime), 1);

			// Close on escape / android back button
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				PanelOpen = false;
			}
		}

		public void OnSliderDay()
		{
			int dayInYear = (int)Math.Round(sliderDayOfYear.value);
			
			dt = new DateTime(dt.Year, 1, 1, dt.Hour, dt.Minute, 0).AddDays(dayInYear - 1);

			textDate.text = dateToString();

			if (sun)
			{
				sun.Date = dt;
			}
		}

		public void OnSliderHour()
		{
			int sliderMinutes = (int)sliderMinuteOfDay.value - 1;

			dt = new DateTime(dt.Year, dt.Month, dt.Day, sliderMinutes / 60, sliderMinutes % 60, 0);

			textTime.text = timeToString();

			if (sun)
			{
				sun.Date = dt;
			}
		}

		String dateToString()
		{
			return dt.Day + " " + months[dt.Month - 1];
		}

		String timeToString()
		{
			String res = String.Empty;
			if (dt.Hour < 10)
			{
				res += "0" + dt.Hour + ":";
			}
			else
			{
				res += dt.Hour + ":";
			}

			if (dt.Minute < 10)
			{
				res += "0" + dt.Minute;
			}
			else
			{
				res += dt.Minute;
			}

			return res;
		}
	}
}
