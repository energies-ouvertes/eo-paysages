﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using UnityEngine.UI;

namespace EOPaysages.UI
{
	///<summary>
	/// Main panel (hamburger menu).
	///</summary>
	public class PanelAR : Panel
	{
		[Header("Instance variables")]

		[Header("Internal Prefab variables")]
		[SerializeField]
		Image buttonAudioZonesImage;
		[SerializeField]
		Image buttonHedgesImage;
		[SerializeField]
		Image buttonTurbinesImage;
		[SerializeField]
		Image buttonNaturalZonesImage;
		[SerializeField]
		Image buttonZIPImage;
		[SerializeField]
		Image buttonPlateformsImage;


		[SerializeField]
		Sprite buttonAudioZonesOn;
		[SerializeField]
		Sprite buttonAudioZonesOff;


		[SerializeField]
		Sprite buttonHedgesOn;
		[SerializeField]
		Sprite buttonHedgesOff;


		[SerializeField]
		Sprite buttonTurbinesOn;
		[SerializeField]
		Sprite buttonTurbinesOff;


		[SerializeField]
		Sprite buttonNaturalZonesOn;
		[SerializeField]
		Sprite buttonNaturalZonesOff;


		[SerializeField]
		Sprite buttonPlateformsOn;
		[SerializeField]
		Sprite buttonPlateformsOff;


		[SerializeField]
		Sprite buttonZIPOn;
		[SerializeField]
		Sprite buttonZIPOff;


		[SerializeField]
		Slider sliderZonesOpacity;
		[SerializeField]
		Slider sliderTurbinesRPM;
		[SerializeField]
		Text sliderTurbinesRPMLabel;

		[SerializeField]
		Slider sliderWindDirection;

		[SerializeField]
		Slider sliderHedgeGrowth;
		[SerializeField]
		Text sliderHedgeGrowthLabel;

		void Start()
		{
			AROverlaysSettings.TurbineHighlightChanged.AddListener(RefreshButtons);
			AROverlaysSettings.HedgesVisibilityChanged.AddListener(RefreshButtons);

			AROverlaysSettings.AudioCirclesVisibilityChanged.AddListener(RefreshButtons);
			AROverlaysSettings.NaturalZonesVisibilityChanged.AddListener(RefreshButtons);
			AROverlaysSettings.PlatformsVisibilityChanged.AddListener(RefreshButtons);
			AROverlaysSettings.ZIPVisibilityChanged.AddListener(RefreshButtons);
			RefreshButtons();

			// Apply initial slider values
			OnSliderZonesOpacity();
			OnSliderTurbinesRPM();
			OnSliderWindDirection();
			OnSliderHedgeGrowth();
			
			AROverlaysSettings.TurbinesHighlight = false;

			PanelOpen = false;
		}

		bool _panelOpen = false;
		public override bool PanelOpen
		{
			get => _panelOpen;
			set
			{
				_panelOpen = value;
				if (value)
				{
					foreach (Panel panel in allPanels)
					{
						if (panel != this)
						{
							panel.PanelOpen = false;
						}
					}
				}
			}
		}

		void Update()
		{
			transform.localScale = new Vector3(1, Mathf.Lerp(transform.localScale.y, PanelOpen ? 1f : 0f, 20f * Time.deltaTime), 1);

			// Close on escape / android back button
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				PanelOpen = false;
			}
		}

		public void OnClickHedges()
		{
			AROverlaysSettings.HedgesVisibility = !AROverlaysSettings.HedgesVisibility;
		}

		public void OnClickTurbines()
		{
			AROverlaysSettings.TurbinesHighlight = !AROverlaysSettings.TurbinesHighlight;
		}

		void OnApplicationQuit()
		{
			AROverlaysSettings.TurbinesHighlight = false;
		}

		public void OnClickAudioZones()
		{
			AROverlaysSettings.AudioCircles = !AROverlaysSettings.AudioCircles;
		}

		public void OnClickNaturalZones()
		{
			AROverlaysSettings.NaturalZones = !AROverlaysSettings.NaturalZones;
		}

		public void OnClickPlatforms()
		{
			AROverlaysSettings.Platforms = !AROverlaysSettings.Platforms;
		}

		public void OnClickZIP()
		{
			AROverlaysSettings.ZIPVisibility = !AROverlaysSettings.ZIPVisibility;
		}

		public void RefreshButtons()
		{
			buttonHedgesImage.sprite = AROverlaysSettings.HedgesVisibility ? buttonHedgesOn : buttonHedgesOff;
			buttonTurbinesImage.sprite = AROverlaysSettings.TurbinesHighlight ? buttonTurbinesOn : buttonTurbinesOff;

			buttonAudioZonesImage.sprite = AROverlaysSettings.AudioCircles ? buttonAudioZonesOn : buttonAudioZonesOff;
			buttonNaturalZonesImage.sprite = AROverlaysSettings.NaturalZones ? buttonNaturalZonesOn : buttonNaturalZonesOff;
			buttonPlateformsImage.sprite = AROverlaysSettings.Platforms ? buttonPlateformsOn : buttonPlateformsOff;
			buttonZIPImage.sprite = AROverlaysSettings.ZIPVisibility ? buttonZIPOn : buttonZIPOff;
		}

		public void OnSliderZonesOpacity()
		{
			AROverlaysSettings.ZonesOpacity = sliderZonesOpacity.value;
		}

		public void OnSliderTurbinesRPM()
		{
			AROverlaysSettings.TurbinesRPM = sliderTurbinesRPM.value;
			sliderTurbinesRPMLabel.text = $"{sliderTurbinesRPM.value,1:F1} tours par minutes";
		}

		public void OnSliderWindDirection()
		{
			AROverlaysSettings.WindDirection = sliderWindDirection.value;
		}

		const float baseHedgeHeight = 1f;
		public void OnSliderHedgeGrowth()
		{
			Shader.SetGlobalFloat("_HEDGE_GROWTH", sliderHedgeGrowth.value);
			sliderHedgeGrowthLabel.text = $"Hauteur des haies: {Mathf.RoundToInt((baseHedgeHeight+sliderHedgeGrowth.value)*100f)}cm";
		}
	}
}
