﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using UnityEngine;

namespace EOPaysages.Location
{
	///<summary>
	/// Sets the WorldOrigin based on GNSS readings.
	///</summary>
	public class DeviceLocation : MonoBehaviour
	{
		///<summary>Ignore the device's reported altitude and use the ground provider instead.</summary>
		public bool StickToGround = false;

		const float groundOffset = 1.7f;

		void Start()
		{
			Input.location.Start(.1f, 5f);
		}

		const float updateDelay = 0.5f;
		float updateTimer = 0;

		void Update()
		{
			updateTimer -= Time.deltaTime;
		
			if (updateTimer <= 0)
			{
				if (Input.location.status == LocationServiceStatus.Running)
				{
					LocationInfo loc = Input.location.lastData;
					Geopoint geopoint = new Geopoint(loc.latitude, loc.longitude, loc.altitude);

					if (StickToGround)
					{
						if (geopoint.Latitude != OriginManager.WorldOrigin.Latitude && geopoint.Longitude != OriginManager.WorldOrigin.Longitude)
						{
							StartCoroutine("stickToGround", geopoint);
						}
					}
					else
					{
						applyLocation(geopoint);
					}
				}

				updateTimer = updateDelay;
			}
		}

		IEnumerator stickToGround(Geopoint geopoint)
		{
			// Block updates
			updateTimer = 120f;

			Debug.Log($"DeviceLocation: Getting ground elevation for {geopoint.Latitude}, {geopoint.Longitude}...");

			yield return ARSceneManager.Instance.ElevationProvider.GetElevationAt(geopoint, (elevation) => {
				applyLocation(new Geopoint(geopoint.Latitude, geopoint.Longitude, elevation + groundOffset));
				updateTimer = updateDelay;
			});
		}

		void applyLocation(Geopoint geopoint)
		{
			if (geopoint != OriginManager.WorldOrigin)
			{
				OriginManager.WorldOrigin = geopoint;
				updateTimer = updateDelay;
			}
		}
	}
}
