﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;
using UnityEngine;

namespace EOPaysages.Location
{
	///<summary>
	/// GNSS coordinate container.
	/// The convention is to leave altitude at 0 if the value is absent.
	///</summary>
	public readonly struct Geopoint
	{
		public readonly double Latitude;

		public readonly double Longitude;

		public readonly double Altitude;

		public static Geopoint Lerp(Geopoint a, Geopoint b, float f)
		{
			return new Geopoint(
				Utils.Lerp(a.Latitude, b.Latitude, f),
				Utils.Lerp(a.Longitude, b.Longitude, f),
				Utils.Lerp(a.Altitude, b.Altitude, f)
			);
		}

		public Geopoint(double latitude, double longitude, double altitude)
		{
			Latitude = latitude;
			Longitude = longitude;
			Altitude = altitude;
		}


		///<summary>
		/// Coroutine to get elevation value from the current Elevation 
		/// provider and get a copy of this Geopoint with the new value.
		///</summary>
		public System.Collections.IEnumerator GetElevationCoroutine(System.Action<Geopoint> valueWithElevation)
		{
			double lat = Latitude;
			double lon = Longitude;
			yield return ARSceneManager.Instance.ElevationProvider.GetElevationAt(this, ((elevation) => 
			{
				valueWithElevation(new Geopoint(lat, lon, elevation));
			}));
		}

		///<summary>
		/// Transforms this Geopoint into cartesian coordinates (meters) 
		/// with X pointing to East, Y up and Z poiting to North,
		/// relatively to a given origin point.
		///</summary>
		public Vector3 RelativeTo(Geopoint origin)
		{
			const int earthRadius = 6378137;
			const double wgs84zFactor = (1.0 - 1.0/298.257223563);

			// This
			double lat1 = Utils.Deg2Rad(Latitude);
			double lon1 = Utils.Deg2Rad(Longitude);
			double alt1 = earthRadius + Altitude;

			double r1x = alt1 * Math.Cos(lat1) * Math.Cos(lon1);
			double r1y = alt1 * Math.Cos(lat1) * Math.Sin(lon1);
			double r1z = alt1 * Math.Sin(lat1) * wgs84zFactor;
			
			// Origin
			double lat2 = Utils.Deg2Rad(origin.Latitude);
			double lon2 = Utils.Deg2Rad(origin.Longitude);
			double alt2 = earthRadius + origin.Altitude;

			double r2x = alt2 * Math.Cos(lat2) * Math.Cos(lon2);
			double r2y = alt2 * Math.Cos(lat2) * Math.Sin(lon2);
			double r2z = alt2 * Math.Sin(lat2) * wgs84zFactor;

			// Up from origin (origin but altitude of 1)
			double latU = lat2;
			double lonU = lon2;

			double rUx = Math.Cos(latU) * Math.Cos(lonU);
			double rUy = Math.Cos(latU) * Math.Sin(lonU);
			double rUz = Math.Sin(latU) * wgs84zFactor;
			Vector3 upDir = new Vector3((float) rUx, (float)rUy, (float)rUz).normalized;

			// North from origin
			double latN = lat2 - 0.001;
			double lonN = lon2;
			double altN = alt2;

			double rNx = altN * Math.Cos(latN) * Math.Cos(lonN);
			double rNy = altN * Math.Cos(latN) * Math.Sin(lonN);
			double rNz = altN * Math.Sin(latN) * wgs84zFactor;

			double dxN = rNx-r2x;
			double dyN = rNy-r2y;
			double dzN = rNz-r2z;
			Vector3 northDir = new Vector3((float)dxN, (float)dyN, (float)dzN).normalized;
			// Make sure north is orthogonal to up
			northDir = Vector3.ProjectOnPlane(northDir, upDir).normalized;


			Quaternion rotation = Quaternion.LookRotation(northDir, upDir);

			double dx = r1x-r2x;
			double dy = r1y-r2y;
			double dz = r1z-r2z;
			Vector3 difference = new Vector3((float)dx, (float)dy, (float)dz);

			Vector3 relativePosition = Quaternion.Inverse(rotation) * difference;

			// Flip Z axis as a temporary fix
			relativePosition.z *= -1;

			return relativePosition;
		}

		public override string ToString()
		{
			return $"Geopoint(Lat: {Latitude} Lon: {Longitude} Alt: {Altitude})";
		}

		public override bool Equals(object obj) => obj is Geopoint other && this.Equals(other);

		public bool Equals(Geopoint g)
		{
			return Latitude == g.Latitude && Longitude == g.Longitude && Altitude == g.Altitude;
		}

		public static explicit operator Geopoint(LocationInfo locationInfo) => new Geopoint(locationInfo.latitude, locationInfo.longitude, locationInfo.altitude);

		public static bool operator ==(Geopoint a, Geopoint b) => a.Equals(b);
		
		public static bool operator !=(Geopoint a, Geopoint b) => !(a == b);

		public override int GetHashCode() => (Latitude, Longitude, Altitude).GetHashCode();
	}
}
