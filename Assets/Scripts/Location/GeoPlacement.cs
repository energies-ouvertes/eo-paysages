﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using UnityEngine;

namespace EOPaysages.Location
{
	///<summary>
	/// Adjusts the position of the object based on a Geopoint. 
	/// The position is updated whenever the OriginManager updates.
	///</summary>
	public class GeoPlacement : MonoBehaviour
	{
		[SerializeField]
		[Tooltip("Ignore geopoint's altitude and use ground elevation instead.")]
		bool stickToGround = true;

		Geopoint _position;
		public Geopoint Position
		{
			get => _position;
			set
			{
				applyLatLon(value);
				refreshAltitude();
			}
		}

		void OnEnable()
		{
			OriginManager.OriginUpdated.AddListener(refreshPosition);

			refreshPosition();
			refreshAltitude();
		}

		void OnDisable()
		{
			OriginManager.OriginUpdated.RemoveListener(refreshPosition);
		}

		///<summary>
		/// Updates the Position property but does not care about stickToGround
		///</summary>
		void applyLatLon(Geopoint value)
		{
			_position = value;
			refreshPosition();
		}

		Option<Coroutine> coroutine = Option<Coroutine>.None;

		void refreshAltitude()
		{
			if (stickToGround)
			{
				if (coroutine.HasValue)
				{
					StopCoroutine(coroutine.Value);
				}
				coroutine = StartCoroutine("runRefreshAltitude");
			}
		}

		IEnumerator runRefreshAltitude()
		{
			Option<double> altitude = Option<double>.None;
			yield return ARSceneManager.Instance.ElevationProvider.GetElevationAt(Position, (elevation => {
				altitude = elevation;
			}));

			while (!altitude.HasValue)
			{
				yield return null;
			}

			applyLatLon(new Geopoint(Position.Latitude, Position.Longitude, altitude.Value));
			Debug.Log($"Geoplacement for {gameObject.name}: New altitude: {altitude.Value}");
		}

		void refreshPosition()
		{
			transform.position = Position.RelativeTo(OriginManager.WorldOrigin);
		}
	}
}
