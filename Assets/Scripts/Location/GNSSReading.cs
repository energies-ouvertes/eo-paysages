// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
namespace EOPaysages.Location
{
	///<summary>
	/// Timestamped geopoint container.
	///</summary>
	public readonly struct GNSSReading
	{
		public readonly Geopoint geopoint;

		///<summary>Seconds since 1970.</summary>
		public readonly double timestamp;

		public GNSSReading(Geopoint geopoint, double timestamp)
		{
			this.geopoint = geopoint;
			this.timestamp = timestamp;
		}

		public static explicit operator GNSSReading(UnityEngine.LocationInfo info) => new GNSSReading((Geopoint)info, info.timestamp);
	}
}
