﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;

namespace EOPaysages
{
	///<summary>
	/// Wind turbine.
	///</summary>
	public class Turbine : MonoBehaviour
	{
		TurbineDimensions _dimensions = TurbineDimensions.defaultDimensions;
		public TurbineDimensions Dimensions
		{
			get => _dimensions;
			set
			{
				_dimensions = value;
				refreshDimensions();
			}
		}

		///<summary>Rotations per minute</summary>
		float rotationSpeed = 30;
		float direction = 0;

		[Header("Prefab variables")]
		[SerializeField]
		Transform tower;

		[SerializeField]
		Transform head;

		[SerializeField]
		Transform bladesRoot;

		[SerializeField]
		Transform blades;

		float bladesRotation = 0;

		void Start()
		{
			refreshDimensions();

			bladesRotation = Random.Range(0, 360);
		}

		void Update()
		{
			rotationSpeed = Mathf.Lerp(rotationSpeed, AROverlaysSettings.TurbinesRPM, 3.0f * Time.deltaTime);
		
			bladesRotation += 360f * (rotationSpeed / 60f) * Time.deltaTime;
			while (bladesRotation < 0f)
			{
				bladesRotation += 360f;
			}
			while (bladesRotation > 360f)
			{
				bladesRotation -= 360f;
			}

			bladesRoot.localRotation = Quaternion.Euler(0, 0, bladesRotation);

			direction = Mathf.LerpAngle(direction, AROverlaysSettings.WindDirection, 4.0f * Time.deltaTime);
			head.rotation = Quaternion.Euler(0, direction, 0);
		}

		const float MESH_BASE_HEIGHT = 100f;
		const float MESH_BASE_DIAMETER = 4f;
		const float MESH_BASE_ROTOR_DIAMETER = 100f;

		void refreshDimensions()
		{
			// Height
			head.localPosition = new Vector3(0, Dimensions.height, 0);
			tower.localScale = new Vector3(Dimensions.diameter / MESH_BASE_DIAMETER, Dimensions.height / MESH_BASE_HEIGHT, Dimensions.diameter / MESH_BASE_DIAMETER);
			head.localScale = tower.localScale.xxx();

			float scale = Dimensions.rotorDiameter / MESH_BASE_ROTOR_DIAMETER;
			blades.localScale = Vector3.one * scale;
		}
	}
}
