// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EOPaysages.Elevation;
using EOPaysages.Location;

namespace EOPaysages
{
	///<summary>
	/// Manages rendering of elevation tiles using instances of GoundTile.
	///</summary>
	[RequireComponent(typeof(TileMeshGenerator))]
	public class Ground : MonoBehaviour
	{
		[Header("Prefab variables")]
		[SerializeField]
		GroundTile tilePrefab;

		TileMeshGenerator tileMeshGenerator;

		Dictionary<ElevationTile, GroundTile> tiles = new Dictionary<ElevationTile, GroundTile>();

		void Start()
		{
			tileMeshGenerator = GetComponent<TileMeshGenerator>();
			loadOrigin();
			OriginManager.OriginUpdated.AddListener(loadOrigin);
		}

		static bool locked = false;

		///<sumamry>
		/// Get altitude at a point. 
		/// This will also add the visual tile if it wasn't loaded yet.
		///</summary>
		public IEnumerator GetAltitudeAt(Geopoint geopoint, System.Action<double> returnValue)
		{
			while (locked)
			{
				yield return null;
			}
			locked = true;

			bool done = false;
			// Make sure we load the tile
			yield return ARSceneManager.Instance.ElevationProvider.GetTileContaining(geopoint, (tile => {
				if (!tiles.ContainsKey(tile))
				{
					Debug.Log($"Ground: Loading {tile.Coordinates}.");
					addTile(tile);
				}

				returnValue(tile.GetElevationAtGeo(geopoint));
				done = true;
				locked = false;
			}));

			while (!done)
			{
				yield return null;
			}
		}


		void loadOrigin()
		{
			StartCoroutine(GetAltitudeAt(OriginManager.WorldOrigin, (tile => {})));
		}
		
		void loadTile(Geopoint geopoint)
		{
			StopCoroutine("loadTileCoroutine");
			StartCoroutine(loadTileCoroutine(geopoint));
		}

		IEnumerator loadTileCoroutine(Geopoint geopoint)
		{
			yield return ARSceneManager.Instance.ElevationProvider.GetTileContaining(geopoint, (tile => {
				addTile(tile);
			}));
		}

		///<summary>
		/// Instantiates a tile and adds it to the tiles dictionary.
		///</summary>
		void addTile(ElevationTile tile)
		{
			if (tiles.ContainsKey(tile))
			{
				Debug.LogError($"Ground tried to instantiate a tile twice: {tile.Coordinates}");
				return;
			}

			GroundTile groundTile = Instantiate<GroundTile>(tilePrefab);
			groundTile.Initialize(tileMeshGenerator);
			groundTile.name = $"GroundTile {tile.Coordinates.X}, {tile.Coordinates.Y} @ {tile.Coordinates.ZoomLevel}";
			groundTile.transform.parent = transform;
			groundTile.Tile = tile;
			tiles.Add(tile, groundTile);
		}
	}
}
