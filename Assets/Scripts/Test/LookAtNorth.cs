// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages.Orientation
{
	///<summary>
	/// Debug object to check if North measurement is accurate.
	///</summary>
	public class LookAtNorth : MonoBehaviour
	{
		const double LATITUDE_OFFSET = 0.000003;

		Vector3 northPos = Vector3.forward;
		
		void Start()
		{
			refreshPosition();
			OriginManager.OriginUpdated.AddListener(refreshPosition);
		}

		void LateUpdate()
		{
			transform.rotation = Quaternion.LookRotation(northPos, Vector3.up);
		}

		void refreshPosition()
		{
			Geopoint newPoint = new Geopoint(OriginManager.WorldOrigin.Latitude + LATITUDE_OFFSET, OriginManager.WorldOrigin.Longitude, OriginManager.WorldOrigin.Altitude);
			northPos = newPoint.RelativeTo(OriginManager.WorldOrigin);
		}
	}
}
