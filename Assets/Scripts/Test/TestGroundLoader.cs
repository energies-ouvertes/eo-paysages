// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections;
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages
{
	///<summary>
	/// Loads a few tiles around the world origin.
	///</summary>
	public class TestGroundLoader : MonoBehaviour
	{
		Ground ground;
		
		void Awake()
		{
			ground = FindObjectOfType<Ground>();
		}

		void Start()
		{
			Invoke("doStuff", 0.25f);
		}

		void doStuff()
		{
			StartCoroutine(doStuffCoroutine());
		}

		IEnumerator doStuffCoroutine()
		{
			yield return ARSceneManager.Instance.ElevationProvider.GetTileContaining(OriginManager.WorldOrigin, (tile => {
				StartCoroutine( bigTest(tile.Coordinates) );
			}));
		}

		IEnumerator bigTest( Elevation.SlippyMapCoords origin )
		{
			for (int x=0; x<5; x++)
			{
				for (int y=0; y<5; y++)
				{
					Elevation.SlippyMapCoords coords = new Elevation.SlippyMapCoords(origin.X + x, origin.Y + y, origin.ZoomLevel);
					Geopoint tileCorner = Utils.SlippyMapToGeopoint( coords );
					Geopoint point = new Geopoint( tileCorner.Latitude + 0.00001, tileCorner.Longitude + 0.00001, 0);
					
					bool gotResult = false;
					yield return StartCoroutine(ground.GetAltitudeAt(point, (altitude => {
						gotResult = true;
					})));

					while (!gotResult)
					{
						yield return null;
					}
				}
			}
		}
	}
}
