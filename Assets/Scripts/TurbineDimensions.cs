// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
namespace EOPaysages
{
	public readonly struct TurbineDimensions
	{
		///<summary>
		/// Length of the tower, in meters.
		///</summary>
		public readonly float height;

		///<summary>
		/// Diameter of the tower, in meters.
		///</summary>
		public readonly float diameter;
		
		///<summary>
		/// Diameter of the rotor (including blades), in meters.
		///</summary>
		public readonly float rotorDiameter;

		public static readonly TurbineDimensions defaultDimensions = new TurbineDimensions(100, 4, 100);

		public TurbineDimensions(float height, float diameter, float rotorDiameter)
		{
			this.height = height;
			this.diameter = diameter;
			this.rotorDiameter = rotorDiameter;
		}

		public override string ToString() => $"TurbineDimensions(tower height: {height}m, tower diameter: {diameter}m, rotor diameter: {rotorDiameter}m)";


		public override int GetHashCode()
		{
			return height.GetHashCode()
						+ 7 * diameter.GetHashCode() 
						+ 29 * rotorDiameter.GetHashCode();
		}

		
		public override bool Equals(object obj) => obj is TurbineDimensions other && this.Equals(other);

		public bool Equals(TurbineDimensions d)
		{
			return height == d.height && diameter == d.diameter && rotorDiameter == d.rotorDiameter;
		}

		public static bool operator ==(TurbineDimensions a, TurbineDimensions b) => a.Equals(b);
		
		public static bool operator !=(TurbineDimensions a, TurbineDimensions b) => !(a == b);
	}
}
