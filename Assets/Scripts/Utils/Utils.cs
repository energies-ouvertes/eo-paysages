// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnitySlippyMap.Helpers;
using EOPaysages.Elevation;
using EOPaysages.Location;

namespace EOPaysages
{
	public static class Utils
	{
#region Geopoint / Slippy map conversions
		///<summary>
		/// Returns the top-left corner of the tile.
		/// Altitude is 0.
		///</summary>
		public static Geopoint SlippyMapToGeopoint(SlippyMapCoords coordinates)
		{
			return SlippyMapToGeopoint(coordinates.X, coordinates.Y, coordinates.ZoomLevel);
		}

		///<summary>
		/// Returns the top-left corner of the tile.
		/// Altitude is 0.
		///</summary>
		public static Geopoint SlippyMapToGeopoint(int x, int y, int zoomLevel)
		{
			double[] lonLat = GeoHelpers.TileToWGS84(x, y, zoomLevel);
			return new Geopoint(lonLat[1], lonLat[0], 0);
		}

		public static SlippyMapCoords GeopointToSlippyMap(Geopoint geopoint, int zoomLevel)
		{
			int[] xy = GeoHelpers.WGS84ToTile(geopoint.Longitude, geopoint.Latitude, zoomLevel);
			return new SlippyMapCoords(xy[0], xy[1], zoomLevel);
		}
#endregion

		///<summary>
		/// Fills an array with a single value.
		///</summary>
		///<returns>
		/// The same array, for chaining.
		///</returns>
		public static T[] Populate<T>(this T[] array, T value)
		{
			for (int i=0; i<array.Length; ++i)
			{
				array[i] = value;
			}
			return array;
		}

#region Math
		///<summary>PI * 2</summary>
		public const double PI2 = System.Math.PI * 2.0;

		public static double Deg2Rad(double angle)
		{
			return System.Math.PI * angle / 180.0;
		}

		public static int Clamp(int value, int min, int max)
		{
			return System.Math.Min(max, System.Math.Max(value, min));
		}

		public static double Clamp(double value, double min, double max)
		{
			return System.Math.Min(max, System.Math.Max(value, min));
		}

		///<summary>
		/// Equivalent of Mathf.Lerp() for doubles.
		///</summary>
		public static double Lerp(double a, double b, double f)
		{
			return (a * (1.0 - f)) + (b * f);
		}

		///<summary>
		/// Equivalent of Mathf.InverseLerp() for doubles.
		///</summary>
		public static double InverseLerp(double a, double b, double value)
		{
			return (value - a) / (b - a);
		}

		///<summary>
		/// Get a line equation parameters in the Y = a * X + b form 
		/// Based on https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
		///</summary>
		public static void GenerateLinearBestFit(IList<Vector2d> points, out double a, out double b, out bool swapped)
		{
			double meanX = points.Average(point => point.x);

			double divisor = points.Sum(point => (point.x - meanX) * (point.x - meanX));

			// If the divisor is too small, the line is (nearly) vertical, 
			// swap X and Y, then recompute meanX and divisor
			if (System.Math.Abs(divisor) <= 0.0000001)
			{
				List<Vector2d> swappedPoints = new List<Vector2d>(points.Count);
				for (int i=0; i<points.Count; i++)
				{
					swappedPoints.Add(points[i].yx);
				}
				points = swappedPoints;
				meanX = points.Average(point => point.x);
				divisor = points.Sum(point => (point.x - meanX) * (point.x - meanX));
				swapped = true;
			}
			else
			{
				swapped = false;
			}

			double meanY = points.Average(point => point.y);

			a = points.Sum(points => (points.x - meanX) * (points.y - meanY)) / divisor;
			b = meanY - a * meanX;
		}
#endregion

#region 2D arrays
		public static int Vec2iToIndex( int x, int y, int maxX )
		{
			return x + (y * maxX);
		}

		public static Vector2Int IndexTo2D( int index, int maxX )
		{
			int y = index / maxX;
			int x = index % maxX;
			return new Vector2Int(x, y);
		}
#endregion

#region Vector swizzling
		// Vector2 -> Vector2
		public static Vector2 xx(this Vector2 v) => new Vector2(v.x, v.x);
		public static Vector2 yy(this Vector2 v) => new Vector2(v.y, v.y);
		public static Vector2 yx(this Vector2 v) => new Vector2(v.y, v.x);

		// Vector3 -> Vector2
		public static Vector2 xx(this Vector3 v) => new Vector2(v.x, v.x);
		public static Vector2 yy(this Vector3 v) => new Vector2(v.y, v.y);
		public static Vector2 zz(this Vector3 v) => new Vector2(v.z, v.z);
		public static Vector2 xy(this Vector3 v) => new Vector2(v.x, v.y);
		public static Vector2 yx(this Vector3 v) => new Vector2(v.y, v.x);
		public static Vector2 xz(this Vector3 v) => new Vector2(v.x, v.z);
		public static Vector2 zx(this Vector3 v) => new Vector2(v.z, v.x);
		public static Vector2 yz(this Vector3 v) => new Vector2(v.y, v.z);
		public static Vector2 zy(this Vector3 v) => new Vector2(v.z, v.y);

		// Vector3 -> Vector3
		public static Vector3 xxx(this Vector3 v) => new Vector3(v.x, v.x, v.x);
		public static Vector3 yyy(this Vector3 v) => new Vector3(v.y, v.y, v.y);
		public static Vector3 zzz(this Vector3 v) => new Vector3(v.z, v.z, v.z);
		public static Vector3 zyx(this Vector3 v) => new Vector3(v.z, v.y, v.x);
#endregion

		///<summary>
		/// Generate a circle. UVs, normals or tangents are not generated.
		///</summary>
		public static Mesh GenerateCircleMesh(int segments)
		{
			segments = System.Math.Max(segments, 3);

			int verticesCount = segments + 2;
			int indicesCount = segments * 3;

			Mesh circle = new Mesh();
			List<Vector3> vertices = new List<Vector3>(verticesCount);
			int[] indices = new int[indicesCount];
			float segmentWidth = Mathf.PI * 2f / segments;
			float angle = 0f;
			vertices.Add(Vector3.zero);
			for (int i = 1; i < verticesCount; ++i)
			{
				vertices.Add(new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle)));
				angle -= segmentWidth;
				if (i > 1)
				{
					int j = (i - 2) * 3;
					indices[j + 0] = 0;
					indices[j + 1] = i - 1;
					indices[j + 2] = i;
				}
			}
			circle.SetVertices(vertices);
			circle.SetIndices(indices, MeshTopology.Triangles, 0);
			circle.RecalculateBounds();
			return circle;
		}

		public static void DestroyObjectChildren(GameObject go)
		{
			DestroyObjectChildren(go.transform);
		}

		public static void DestroyObjectChildren(Transform transform)
		{
			foreach (Transform child in transform)
			{
				GameObject.Destroy(child.gameObject);
			}
		}

		///<summary>
		/// Directory used to store screenshots on the current platform.
		///</summary>
#if UNITY_ANDROID && !UNITY_EDITOR
		public static string ScreenshotDirectory
		{
			get 
			{
				if (pathDCIM == null)
				{
					AndroidJavaClass jc = new AndroidJavaClass("android.os.Environment");
					pathDCIM = jc.CallStatic<AndroidJavaObject>("getExternalStoragePublicDirectory", 
						jc.GetStatic<string>("DIRECTORY_DCIM"))
						.Call<string>("getAbsolutePath");
				}
				return pathDCIM;
			}
		}
		static string pathDCIM = null;
#else
		public static string ScreenshotDirectory => Application.persistentDataPath;
#endif
	}
}
