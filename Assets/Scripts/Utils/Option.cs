// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;

namespace EOPaysages
{
	///<summary>
	/// Equivalent of Rusts's <c>Option</c> or Java's <c>Optional</c>.
	///</summary>
	public readonly struct Option<T>
	{
		public bool HasValue { get; }

		readonly T value;
		public T Value
		{
			get
			{
				if (HasValue)
				{
					return value;
				}
				else
				{
					throw new InvalidOperationException("Attempting to acces value of an empty Option");
				}
			}
		}

		static readonly Option<T> _none = new Option<T>(false, default(T));
		public static Option<T> None => _none;

		public static Option<T> Some(T value)
		{
			return new Option<T>(true, value);
		}

		Option(bool hasValue, T value)
		{
			if (hasValue && value == null)
			{
				throw new NullReferenceException("Attempting to create a non-empty Option with a null value");
			}
			this.HasValue = hasValue;
			this.value = value;
		}

		public T ValueOrDefault(T defaultValue)
		{
			if (HasValue)
			{
				return Value;
			}
			else
			{
				return defaultValue;
			}
		}

		public override int GetHashCode()
		{
			if (HasValue)
			{
				return value.GetHashCode();
			}
			else
			{
				return 0;
			}
		}

		public static implicit operator Option<T>(T value) => Some(value);

		public override string ToString()
		{
			if (HasValue)
			{
				return $"Option.Some({value.ToString()})";
			}
			else
			{
				return "Option.None";
			}
		}

		public override bool Equals(object obj)
		{
			if (obj is Option<T>)
				return this.Equals((Option<T>)obj);
			else
				return false;
		}

		public bool Equals(Option<T> other)
		{
			if (HasValue && other.HasValue)
				return object.Equals(value, other.value);
			else
				return HasValue == other.HasValue;
		}

		public static bool operator ==(Option<T> a, Option<T> b) => a.Equals(b);
		public static bool operator !=(Option<T> a, Option<T> b) => !(a == b);
	}
}
