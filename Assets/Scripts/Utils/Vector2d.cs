// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;

namespace EOPaysages
{
	///<summary>
	/// Double precision equivalent of <c>Vector2</c>
	///</summary>
	public struct Vector2d
	{
		public double x;
		public double y;

		public double sqrMagnitude => x * x + y * y;

		public double magnitude => Math.Sqrt(sqrMagnitude);

#region Swizzling
		public Vector2d xx => new Vector2d(x, x);
		public Vector2d yy => new Vector2d(y, y);
		public Vector2d yx => new Vector2d(y, x);
#endregion

		public Vector2d normalized
		{
			get
			{
				Vector2d v = new Vector2d(x, y);
				v.Normalize();
				return v;
			}
		}

		public static Vector2d zero => new Vector2d(0.0, 0.0);
		public static Vector2d one => new Vector2d(1.0, 1.0);

		public Vector2d(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public void Normalize()
		{
			double mag = magnitude;
			if (mag < Double.Epsilon)
			{
				this = zero;
			}
			else
			{
				this /= mag;
			}
		}

		///<summary>
		/// Returns the angle in degrees between /from/ and /to/.
		///</summary>
		public static double Angle(Vector2d from, Vector2d to)
		{
			double denominator = Math.Sqrt(from.sqrMagnitude * to.sqrMagnitude);
			if (denominator < (1e-15f))
				return 0.0;

			double dot = Utils.Clamp(Dot(from, to) / denominator, -1F, 1F);
			return Math.Acos(dot) * UnityEngine.Mathf.Rad2Deg;
		}

		///<summary>
		/// Returns the signed angle in degrees between /from/ and /to/. Always returns the smallest possible angle
		///</summary>
		public static double SignedAngle(Vector2d from, Vector2d to)
		{
			double unsigned_angle = Angle(from, to);
			double sign = Math.Sign(from.x * to.y - from.y * to.x);
			return unsigned_angle * sign;
		}

		public static double Dot(Vector2d a, Vector2d b)
		{
			return a.x * b.x + a.y * b.y;
		}

		///<summary>
		/// Returns the distance between /a/ and /b/.
		///</summary>
		public static double Distance(Vector2d a, Vector2d b)
		{
			double diffX = a.x - b.x;
			double diffY = a.y - b.y;
			return Math.Sqrt(diffX * diffX + diffY * diffY);
		}

#region operators
		public static explicit operator Vector2d(UnityEngine.Vector2 v) => new Vector2d(v.x, v.y);

		public static explicit operator UnityEngine.Vector2(Vector2d v) => new UnityEngine.Vector2((float)v.x, (float)v.y);

		public static Vector2d operator+(Vector2d a, Vector2d b) => new Vector2d(a.x + b.x, a.y + b.y);

		public static Vector2d operator-(Vector2d a, Vector2d b) => new Vector2d(a.x - b.x, a.y - b.y);

		public static Vector2d operator-(Vector2d v) => new Vector2d(-v.x, -v.y);

		public static Vector2d operator*(Vector2d v, double d) => new Vector2d(v.x * d, v.y * d);

		public static Vector2d operator/(Vector2d v, double d) => new Vector2d(v.x / d, v.y / d);

		public static bool operator==(Vector2d a, Vector2d b)
		{
			// Returns false in the presence of NaN values.
			double diff_x = a.x - b.x;
			double diff_y = a.y - b.y;
			return (diff_x * diff_x + diff_y * diff_y) < Double.Epsilon;
		}
		public static bool operator!=(Vector2d a, Vector2d b) => !(a == b);
#endregion

		public override bool Equals(object other)
		{
			if (!(other is Vector2d)) return false;

			return Equals((Vector2d)other);
		}

		public bool Equals(Vector2d other) => x == other.x && y == other.y;

		public override int GetHashCode() => x.GetHashCode() ^ (y.GetHashCode() << 2);

		public override string ToString() => $"({x}, {y})";

		public string ToString(string format) => $"({x.ToString(format)}, {y.ToString(format)})";

		public string ToString(IFormatProvider provider) => $"({x.ToString(provider)}, {y.ToString(provider)})";

		public string ToString(string format, IFormatProvider provider) => $"({x.ToString(format, provider)}, {y.ToString(format, provider)})";
	}
}
