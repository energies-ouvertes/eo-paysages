// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace EOPaysages
{
	public class Screenshoter : MonoBehaviour
	{
		///<summary>
		/// Saves a screenshot as a PNG file, along with the raw camera picture
		/// and a metadata file containing location, orientation and a list of visible scenarios.
		///</summary>
		public void Screenshot()
		{
			string filename = Application.productName.ToLowerInvariant().Replace(' ', '_'); 
			filename += "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss");
			string dir = Utils.ScreenshotDirectory;
			string path_meta = Path.Combine(dir, filename + ".json");
			string path_pic = Path.Combine(dir, filename);

			Debug.Log("Saving screenshot at: " + path_pic);

			StartCoroutine("saveScreenshot", path_pic);

			Camera cam = ARSceneManager.Instance.MainCamera;
			Vector3 camRotation = cam.transform.eulerAngles;

			// Build list of titles of visible scenarios
			List<String> visibleScenarios = new List<String>(1);
			foreach (String path in ARSceneManager.Instance.ScenariosManager.LoadedScenarios)
			{
				if (ARSceneManager.Instance.ScenariosManager.ScenarioVisibility(path).ValueOrDefault(false))
				{
					Option<Scenario.ScenarioDescriptor> descriptor = ARSceneManager.Instance.ScenariosManager.GetScenario(path);
					if (descriptor.HasValue )
					{
						visibleScenarios.Add(descriptor.Value.title);
					}
				}
			}

			ScreenshotMetadata metadata = new ScreenshotMetadata
			{
				latitude = Location.OriginManager.WorldOrigin.Latitude,
				longitude = Location.OriginManager.WorldOrigin.Longitude,

				yaw = camRotation.y,
				pitch = camRotation.x,
				roll = camRotation.z,

				vertical_field_of_view = cam.fieldOfView,

				scenarios = visibleScenarios,
			};
			string metadataStr = JsonUtility.ToJson(metadata, true);
			File.WriteAllText(path_meta, metadataStr);

			UI.Alerts.Instance.Alert("Capture d'écran sauvegardée");
		}

		IEnumerator saveScreenshot(string path)
		{
			Camera cam = ARSceneManager.Instance.MainCamera;

			Canvas[] canvases = FindObjectsOfType<Canvas>();
			bool[] enabledCanvases = new bool[canvases.Length];

			for (int i=0; i<canvases.Length; i++)
			{
				enabledCanvases[i] = canvases[i].enabled;
				canvases[i].enabled = false;
			}

			yield return new WaitForEndOfFrame();

			// Create a texture the size of the screen, RGB24 format
			int width = cam.pixelWidth;
			int height = cam.pixelHeight;
			Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);

			// Read screen contents into the texture
			tex.ReadPixels(new Rect (0, 0, width, height), 0, 0);
			tex.Apply();

			// Restore canvases visibility
			for (int i=0; i<canvases.Length; i++)
			{
				canvases[i].enabled = enabledCanvases[i];
			}

			byte[] bytes = tex.EncodeToPNG();
			File.WriteAllBytes(path + ".png", bytes);

			// Raw camera image
			ARCameraBackground arCameraBackground = cam.GetComponent<ARCameraBackground>();
			if (arCameraBackground != null)
			{
				RenderTexture cameraRenderTexture = RenderTexture.GetTemporary(width, height);
				// Copy the camera background to a RenderTexture
				Graphics.Blit(null, cameraRenderTexture, arCameraBackground.material);
				
				// Copy the RenderTexture from GPU to CPU
				RenderTexture previousRenderTexture = RenderTexture.active;
				RenderTexture.active = cameraRenderTexture;
				tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
				tex.Apply();

				// Restore render texture
				RenderTexture.active = previousRenderTexture;
				
				// Write to file
				bytes = tex.EncodeToPNG();
				File.WriteAllBytes(path + "_photo.png", bytes);
			}

			// Clear memory
			Destroy(tex);
		}

		[Serializable]
		struct ScreenshotMetadata
		{
			public double latitude;
			public double longitude;

			public float yaw;
			public float pitch;
			public float roll;

			public float vertical_field_of_view;

			public List<String> scenarios;
		}
	}
}
