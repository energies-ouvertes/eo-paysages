﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages
{
	public class MeshGenerator : MonoBehaviour
	{
		Mesh mesh;

		[Header("Internal prefab variables")]
		[SerializeField]
		private Material TestMaterial;

		[SerializeField]
		private GameObject template;

		void Start()
		{
# if UNITY_EDITOR
			// Test mesh
			Geopoint[] testPoints = new Geopoint[]
			{
				new Geopoint(-1.90732725476789, 47.7777221282616, 0),
				new Geopoint(-1.90735164327499, 47.777499346139, 0),
				new Geopoint(-1.90727597146145, 47.7774841302425, 0),
				new Geopoint(-1.9072774289848, 47.7774773267946, 0),
				new Geopoint(-1.90756083536901, 47.7774743557922, 0),
				new Geopoint(-1.90779249517915, 47.777642402499, 0),
				new Geopoint(-1.90822788823073, 47.7780383382174, 0),
				new Geopoint(-1.90872649710102, 47.7785422738993, 0),
				new Geopoint(-1.90851217099039, 47.7787265694434, 0),
				new Geopoint(-1.90836899657462, 47.7787097252389, 0),
				new Geopoint(-1.90817290286793,47.7785771528597, 0),
				new Geopoint(-1.90779718340992,47.778245223808, 0),
				new Geopoint(-1.90736620393532,47.7777682351641, 0),
				new Geopoint(-1.90732725476789,47.7777221282616, 0),
			};
			updateMeshes( new Geopoint[][]{testPoints} );
#endif
		}

		public void updateMeshes(IEnumerable<IEnumerable<Geopoint>> geopointlist)
		{
			foreach (IEnumerable<Geopoint> gpl in geopointlist)
			{
				createMesh(gpl);
			}
		}

		void createMesh(IEnumerable<Geopoint> gp)
		{
			bool first = true;
			Vector2[] vertices2D;
			List<Vector3> vertices = new List<Vector3>(gp.Count());
			Geopoint reference = new Geopoint();

			Triangulator tr;
			int[] indices;

			// Create vertices list for one polygon
			foreach (Geopoint geopoint in gp)
			{
				if (first)
				{
					reference = geopoint;
					vertices.Add(Vector3.zero);
					first = false;
				}
				else
				{
					// Create the vertices relative to the first point
					vertices.Add(geopoint.RelativeTo(reference));
				}
			}

			
			mesh = new Mesh();
			mesh.vertices = vertices.ToArray();

			// Create the 2D vertices for the triangulator
			vertices2D = new Vector2[vertices.Count];
			for (int i = 0; i < vertices2D.Length; i++)
			{
				vertices2D[i] = mesh.vertices[i].xz();
			}

			// Make sure Normals of mesh is facing up
			Vector3[] normals = Enumerable.Repeat(Vector3.up, vertices.Count).ToArray();
			mesh.normals = normals;

			// Use the triangulator to get the indices for creating triangles
			tr = new Triangulator(vertices2D);
			indices = tr.Triangulate();

			// TODO: Only do this if necessary
			// Flip mesh
			Array.Reverse(indices);

			// Set indices
			mesh.triangles = indices;


			mesh.RecalculateBounds();
			mesh.MarkModified();

			mesh.name = "GeneratedZoneMesh";

			// Instantiate mesh
			GameObject zone = Instantiate(template, Vector3.zero, Quaternion.identity);
			zone.transform.SetParent(gameObject.transform);

			// Set up mesh data;
			MeshRenderer renderer = zone.GetComponent<MeshRenderer>();
			MeshFilter filter = zone.GetComponent<MeshFilter>();
			// renderer.material = TestMaterial;
			// renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			filter.mesh = mesh;

			GeoPlacement placement = zone.AddComponent<GeoPlacement>();
			placement.Position = reference;
		}
	}
}
