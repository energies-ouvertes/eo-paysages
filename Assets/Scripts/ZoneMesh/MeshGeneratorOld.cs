﻿// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (C) 2021 ÉO
// This file is part of ÉO Paysages.

// ÉO Paysages is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// ÉO Paysages is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with ÉO Paysages.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using EOPaysages.Location;

namespace EOPaysages
{
	///<summary>
	/// Legacy code.
	///</summary>
	public class MeshGeneratorOld : MonoBehaviour
	{
		public Material TestMaterial;

		Mesh mesh;

		Vector2[] vertices2D;

		List<Vector3> vertices = new List<Vector3>();

		string[] vegetation;
		Geopoint reference;

		bool first;


		Triangulator tr;
		int[] indices;

		void Start()
		{
			first = true;

			//need a function to return coord list

#region Vegetation zone coordinates
			vegetation = new string[]{"-1.90732725476789, 47.7777221282616",
				"-1.90735164327499, 47.777499346139",
				"-1.90727597146145, 47.7774841302425",
				"-1.9072774289848, 47.7774773267946",
				"-1.90756083536901, 47.7774743557922",
				"-1.90779249517915, 47.777642402499",
				"-1.90822788823073, 47.7780383382174",
				"-1.90872649710102, 47.7785422738993",
				"-1.90851217099039, 47.7787265694434",
				"-1.90836899657462, 47.7787097252389",
				"-1.90817290286793,47.7785771528597",
				"-1.90779718340992,47.778245223808",
				"-1.90736620393532,47.7777682351641",
				"-1.90732725476789,47.7777221282616"};
#endregion

			foreach (string v in vegetation)
			{
				if (first)
				{
					reference = createGeopoint(v);
					vertices.Add(Vector3.zero);
					first = false;
				}
				else
				{
					// Create the vertices relative to the first point
					vertices.Add(reference.RelativeTo(createGeopoint(v)));
				}
			}
			// foreach (Vector3 v in vertices)
			// {
			// 	Debug.Log(v.ToString());
			// }
			updateMesh();
		}


		Geopoint createGeopoint(string s)
		{
			var coordSplit = s.Split(',');
			var nfi = NumberFormatInfo.InvariantInfo;
			double lon = double.Parse(coordSplit[0], nfi);
			double lat = double.Parse(coordSplit[1], nfi);
			return new Geopoint(lat, lon, 0);
		}


		void updateMesh()
		{
			mesh = new Mesh();
			mesh.vertices = vertices.ToArray();

			// Create the 2D vertices for the triangulator
			vertices2D = new Vector2[vertices.Count];
			for (int i = 0; i < vertices2D.Length; i++)
			{
				vertices2D[i] = mesh.vertices[i].xz();
			}

			// Make sure Normals of mesh is facing up
			Vector3[] normals = Enumerable.Repeat(Vector3.up, vertices.Count).ToArray();
			mesh.normals = normals;

			// Use the triangulator to get the indices for creating triangles
			tr = new Triangulator(vertices2D);
			indices = tr.Triangulate();

			// TODO: Only do this if necessary
			// Flip mesh
			Array.Reverse(indices);

			// Set indices
			mesh.triangles = indices;


			mesh.RecalculateBounds();
			mesh.MarkModified();

			// Set up game object with mesh;
			MeshRenderer renderer = gameObject.AddComponent<MeshRenderer>();
			MeshFilter filter = gameObject.AddComponent<MeshFilter>();
			renderer.material = TestMaterial;
			renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			filter.mesh = mesh;
		}
	}
}
