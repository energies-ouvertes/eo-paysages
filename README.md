# ÉO Paysages

Application de prévisualisation de parcs éoliens en réalité augmentée pour Android utilisant ARCore.

Cette application utilise des fichiers de scénarios créés par l'[éditeur de scénarios](https://gitlab.com/energies-ouvertes/eo-paysages-editeur).

[Trello board](https://trello.com/b/1vFxQ01G)

## Clefs d'API

Les clefs d'API sont stockées dans des fichiers `.txt` dans le dossier `Assets/Resources/Secret/`. Ces clefs ne devant pas êtres partagées, ce dossier est ignoré par Git. L'application accède à ces clefs via la classe `SecretTextsManager`.

 - Clef [Mapbox](https://www.mapbox.com/) (pour déterminer l'élévation du terrain) : `Assets/Resources/Secret/mapbox.txt`

